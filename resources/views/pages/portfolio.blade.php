<!DOCTYPE HTML>
<html lang="en" ontouchmove>
    
<!-- Mirrored from outdoor.kwst.net/site/portfolio.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 14 Apr 2021 11:30:04 GMT -->
<head>
        <!--=============== basic  ===============-->
        <meta charset="UTF-8">
        <title>Outdoor  - Responsive  Photography Template</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="robots" content="index, follow"/>
        <meta name="keywords" content=""/>
        <meta name="description" content=""/>
        <!--=============== css  ===============-->
        <link type="text/css" rel="stylesheet" href="css/reset.css">
        <link type="text/css" rel="stylesheet" href="css/plugins.css">
        <link type="text/css" rel="stylesheet" href="css/style.css">
        <link type="text/css" rel="stylesheet" href="css/yourstyle.css">
        <!--=============== favicons ===============-->
        <link rel="shortcut icon" href="img/favicon.ico">
    </head>
    <body>
        <!--Loader  -->
        <div class="loader"><i class="fa fa-refresh fa-spin"></i></div>
        <!--LOader end  -->
        <!--================= main start ================-->
        <div id="main">
            <!--=============== header ===============-->
            @include('includes.header')
            <!--header end -->
            <!--=============== wrapper ===============-->
            <div id="wrapper">
                <!--=============== Conten holder  ===============-->
                <div class="content-holder elem scale-bg2 transition3" >
                    <!--=============== Content  ===============-->
                    <div class="content full-height">
                        <div class="fixed-title"><span>Portfolio</span></div>
                        <!-- Portfolio counter  -->
                        <div class="count-folio">
                            <div class="num-album"></div>
                            <div class="all-album"></div>
                        </div>
                        <!-- Portfolio counter end -->
                        <div class="filter-holder column-filter">
                            <div class="filter-button">Filter <i class="fa fa-long-arrow-down"></i></div>
                            <div class="gallery-filters hid-filter">
                                <a href="#" class="gallery-filter transition2 gallery-filter_active" data-filter="*">All Albums</a>
                                <a href="#" class="gallery-filter transition2" data-filter=".people">People</a>
                                <a href="#" class="gallery-filter transition2" data-filter=".nature">Nature</a>
                                <a href="#" class="gallery-filter transition2" data-filter=".comercial">Comercial</a>
                                <a href="#" class="gallery-filter transition2" data-filter=".travel">Travel</a>
                            </div>
                        </div>
                        <!--=============== portfolio holder ===============-->
                        <div class="resize-carousel-holder">
                            <div class="p_horizontal_wrap">
                                <div id="portfolio_horizontal_container">
                                    <!-- portfolio item -->
                                    <div class="portfolio_item people comercial">
                                        <img  src="img/bg/22.jpg"   alt="">
                                        <div class="port-desc-holder">
                                            <div class="port-desc">
                                                <div class="overlay"></div>
                                                <div class="grid-item">
                                                    <h3><a href="portfolio-single.php">Quisque non augue</a></h3>
                                                    <span>Travel</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="port-subtitle-holder">
                                            <div class="port-subtitle">
                                                <h3><a href="portfolio-single.php">Quisque non augue</a></h3>
                                                <span><a href="#">Travel</a> / <a href="#">Photography</a></span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- portfolio item end -->
                                    <!-- portfolio item -->
                                    <div class="portfolio_item travel nature">
                                        <img  src="img/bg/47.jpg"   alt="">
                                        <div class="port-desc-holder">
                                            <div class="port-desc">
                                                <div class="overlay"></div>
                                                <div class="grid-item">
                                                    <h3><a href="portfolio-single.php">Curabitur bibendum</a></h3>
                                                    <span>Travel</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="port-subtitle-holder">
                                            <div class="port-subtitle">
                                                <h3><a href="portfolio-single.php">Curabitur bibendum</a></h3>
                                                <span><a href="#">Travel</a> / <a href="#">Photography</a></span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- portfolioitem end -->
                                    <!-- portfolio item -->
                                    <div class="portfolio_item travel">
                                        <img  src="img/bg/33.jpg"   alt="">
                                        <div class="port-desc-holder">
                                            <div class="port-desc">
                                                <div class="overlay"></div>
                                                <div class="grid-item">
                                                    <h3><a href="portfolio-single.php">Adipiscing elit</a></h3>
                                                    <span>Travel</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="port-subtitle-holder">
                                            <div class="port-subtitle">
                                                <h3><a href="portfolio-single.php">Adipiscing elit</a></h3>
                                                <span><a href="#">Travel</a> / <a href="#">Photography</a></span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- portfolio item end -->
                                    <!-- portfolio item -->
                                    <div class="portfolio_item nature comercial">
                                        <img  src="img/bg/19.jpg"   alt="">
                                        <div class="port-desc-holder">
                                            <div class="port-desc">
                                                <div class="overlay"></div>
                                                <div class="grid-item">
                                                    <h3><a href="portfolio-single.php">Nam sagittis pretium</a></h3>
                                                    <span>Nature</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="port-subtitle-holder">
                                            <div class="port-subtitle">
                                                <h3><a href="portfolio-single.php">Nam sagittis pretium</a></h3>
                                                <span><a href="#">Travel</a> / <a href="#">Nature</a></span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- portfolioitem end -->
                                    <!-- portfolio item -->
                                    <div class="portfolio_item travel comercial">
                                        <img  src="img/bg/9.jpg"   alt="">
                                        <div class="port-desc-holder">
                                            <div class="port-desc">
                                                <div class="overlay"></div>
                                                <div class="grid-item">
                                                    <h3><a href="portfolio-single.php">Nam gravida</a></h3>
                                                    <span>Travel</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="port-subtitle-holder">
                                            <div class="port-subtitle">
                                                <h3><a href="portfolio-single.php">Nam gravida</a></h3>
                                                <span><a href="#">Travel</a> / <a href="#">Photography</a></span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- portfolio item end -->
                                    <!-- portfolio item -->
                                    <div class="portfolio_item travel comercial">
                                        <img  src="img/bg/3.jpg"   alt="">
                                        <div class="port-desc-holder">
                                            <div class="port-desc">
                                                <div class="overlay"></div>
                                                <div class="grid-item">
                                                    <h3><a href="portfolio-single.php">Justo tortor</a></h3>
                                                    <span>Travel</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="port-subtitle-holder">
                                            <div class="port-subtitle">
                                                <h3><a href="portfolio-single.php">Justo tortor</a></h3>
                                                <span><a href="#">Travel</a> / <a href="#">Photography</a></span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- portfolio item end -->
                                    <!-- portfolio item -->
                                    <div class="portfolio_item people nature">
                                        <img  src="img/bg/26.jpg"   alt="">
                                        <div class="port-desc-holder">
                                            <div class="port-desc">
                                                <div class="overlay"></div>
                                                <div class="grid-item">
                                                    <h3><a href="portfolio-single.php">Integer euismod</a></h3>
                                                    <span>Travel</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="port-subtitle-holder">
                                            <div class="port-subtitle">
                                                <h3><a href="portfolio-single.php">Integer euismod</a></h3>
                                                <span><a href="#">Travel</a> / <a href="#">Photography</a></span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- portfolio item end -->
                                    <!-- portfolio item -->
                                    <div class="portfolio_item travel nature">
                                        <img  src="img/bg/55.jpg"   alt="">
                                        <div class="port-desc-holder">
                                            <div class="port-desc">
                                                <div class="overlay"></div>
                                                <div class="grid-item">
                                                    <h3><a href="portfolio-single.php">Donec nulla purus,</a></h3>
                                                    <span>Travel</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="port-subtitle-holder">
                                            <div class="port-subtitle">
                                                <h3><a href="portfolio-single.php">Donec nulla purus</a></h3>
                                                <span><a href="#">Travel</a> / <a href="#">Photography</a></span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- portfolio item end -->
                                    <!-- portfolio item -->
                                    <div class="portfolio_item people nature">
                                        <img  src="img/bg/17.jpg"   alt="">
                                        <div class="port-desc-holder">
                                            <div class="port-desc">
                                                <div class="overlay"></div>
                                                <div class="grid-item">
                                                    <h3><a href="portfolio-single.php">Maecenas vitae semper</a></h3>
                                                    <span>Travel</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="port-subtitle-holder">
                                            <div class="port-subtitle">
                                                <h3><a href="portfolio-single.php">Maecenas vitae semper</a></h3>
                                                <span><a href="#">Travel</a> / <a href="#">Photography</a></span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- portfolio item end -->
                                    <!-- portfolio item -->
                                    <div class="portfolio_item people comercial">
                                        <img  src="img/bg/21.jpg"   alt="">
                                        <div class="port-desc-holder">
                                            <div class="port-desc">
                                                <div class="overlay"></div>
                                                <div class="grid-item">
                                                    <h3><a href="portfolio-single.php">Proin iaculis felis </a></h3>
                                                    <span>Travel</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="port-subtitle-holder">
                                            <div class="port-subtitle">
                                                <h3><a href="portfolio-single.php">Proin iaculis felis </a></h3>
                                                <span><a href="#">Travel</a> / <a href="#">Photography</a></span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- portfolio item end -->
                                </div>
                                <!--portfolio_horizontal_container  end-->
                            </div>
                            <!--p_horizontal_wrap  end-->
                        </div>
                    </div>
                    <!-- Content end  -->
                    <!-- Share container  -->
                    <div class="share-container  isShare"  data-share="['facebook','pinterest','googleplus','twitter','linkedin']"></div>
                </div>
                <!-- content holder end -->
            </div>
            <!-- wrapper end -->
            <div class="left-decor"></div>
            <div class="right-decor"></div>
            <!--=============== Footer ===============-->
            @include('includes.footer')
            <!-- footer end -->
        </div>
        <!-- Main end -->
        <!--=============== scripts  ===============-->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugins.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>
    </body>

<!-- Mirrored from outdoor.kwst.net/site/portfolio.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 14 Apr 2021 11:32:06 GMT -->
</html>