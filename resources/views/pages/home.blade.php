<!DOCTYPE HTML>
<html lang="en">
    
<!-- Mirrored from outdoor.kwst.net/site/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 14 Apr 2021 11:27:20 GMT -->
<head>
        <!--=============== basic  ===============-->
        <meta charset="UTF-8">
        <title>Maximusi</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="robots" content="index, follow"/>
        <meta name="keywords" content=""/>
        <meta name="description" content=""/>
        <!--=============== css  ===============-->
        <link type="text/css" rel="stylesheet" href="css/reset.css">
        <link type="text/css" rel="stylesheet" href="css/plugins.css">
        <link type="text/css" rel="stylesheet" href="css/style.css">
        <link type="text/css" rel="stylesheet" href="css/yourstyle.css">
        <!--=============== favicons ===============-->
        <link rel="shortcut icon" href="img/favicon.ico">
    </head>
    <body>
        <!--Loader  -->
        <div class="loader"><i class="fa fa-refresh fa-spin"></i></div>
        <!--LOader end  -->
        <!--================= main start ================-->
        <div id="main">
            <!--=============== header ===============-->
            @include('includes.header')
            <!--header end -->
            <!--=============== wrapper ===============-->
            <div id="wrapper">
                <!--=============== Content holder  ===============-->
                <div class="content-holder elem scale-bg2 transition3 slid-hol" >
                    <!-- Fixed title  -->
                    <div class="fixed-title"><span>Home</span></div>
                    <!-- Fixed title end -->
                    <!--=============== Content ===============-->
                    <div class="content full-height">
                        <!-- full-height-wrap end  -->
                        <div class="full-height-wrap">
                            <div class="swiper-container" id="horizontal-slider" data-mwc="1" data-mwa="0">
                                <div class="swiper-wrapper">
                                    <!--=============== 1 ===============-->
                                    <div class="swiper-slide">
                                        <div class="bg" style="background-image:url(img/bg/22.jpg)"></div>
                                        <div class="overlay"></div>
                                        <div class="zoomimage"><img src="img/bg/22.jpg" class="intense" alt=""><i class="fa fa-expand"></i></div>
                                        <div class="slide-title-holder">
                                            <div class="slide-title">
                                                <span class="subtitle">At posuere sem accumsan </span>
                                                <div class="separator-image"><img src="img/separator.png" alt=""></div>
                                                <h3 class="transition">  <a href="portfolio-single.php">Blandit praesent</a></h3>
                                                <h4><a  href="portfolio-single.php">View</a></h4>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- 1 end -->
                                    <!--=============== 2 ===============-->
                                    <div class="swiper-slide">
                                        <div class="bg" style="background-image:url(img/bg/4.jpg)"></div>
                                        <div class="overlay"></div>
                                        <div class="zoomimage"><img src="img/bg/4.jpg" class="intense" alt=""><i class="fa fa-expand"></i></div>
                                        <div class="slide-title-holder">
                                            <div class="slide-title">
                                                <span class="subtitle">At posuere sem accumsan </span>
                                                <div class="separator-image"><img src="img/separator.png" alt=""></div>
                                                <h3 class="transition">  <a href="portfolio-single.php">In tortor neque</a>  </h3>
                                                <h4><a  href="portfolio-single.php">View</a></h4>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- 2 end -->
                                    <!--=============== 3 ===============-->
                                    <div class="swiper-slide">
                                        <div class="bg" style="background-image:url(img/bg/8.jpg)"></div>
                                        <div class="overlay"></div>
                                        <div class="zoomimage"><img src="img/bg/8.jpg" class="intense" alt=""><i class="fa fa-expand"></i></div>
                                        <div class="slide-title-holder">
                                            <div class="slide-title">
                                                <span class="subtitle">At posuere sem accumsan </span>
                                                <div class="separator-image"><img src="img/separator.png" alt=""></div>
                                                <h3 class="transition">  <a  href="portfolio-single.php">Vestibulum tincidunt</a>  </h3>
                                                <h4><a  href="portfolio-single.php">View</a></h4>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- 3 end -->
                                    <!--=============== 4 ===============-->
                                    <div class="swiper-slide">
                                        <div class="bg" style="background-image:url(img/bg/46.jpg)"></div>
                                        <div class="overlay"></div>
                                        <div class="zoomimage"><img src="img/bg/46.jpg" class="intense" alt=""><i class="fa fa-expand"></i></div>
                                        <div class="slide-title-holder">
                                            <div class="slide-title">
                                                <span class="subtitle">At posuere sem accumsan </span>
                                                <div class="separator-image"><img src="img/separator.png" alt=""></div>
                                                <h3 class="transition">  <a  href="portfolio-single.php">Libero bibendum</a>  </h3>
                                                <h4><a  href="portfolio-single.php">View</a></h4>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- 4 end -->
                                </div>
                            </div>
                            <!-- slider  pagination -->
                            <div class="pagination"></div>
                            <!-- pagination  end -->
                            <!-- slider navigation  -->
                            <div class="swiper-nav-holder hor hs">
                                <a class="swiper-nav arrow-left transition " href="#"><i class="fa fa-angle-left"></i></a>
                                <a class="swiper-nav  arrow-right transition" href="#"><i class="fa fa-angle-right"></i></a>
                            </div>
                            <!-- slider navigation  end -->
                        </div>
                        <!-- full-height-wrap end  -->
                    </div>
                    <!-- Content end  -->
                    <!-- Share container  -->
                    <div class="share-container  isShare"  data-share="['facebook','pinterest','googleplus','twitter','linkedin']"></div>
                </div>
                <!-- content holder end -->
            </div>
            <!-- wrapper end -->
            <div class="left-decor"></div>
            <div class="right-decor"></div>
            <!--=============== Footer ===============-->
            @include('includes.footer')
            <!-- footer end -->
        </div>
        <!-- Main end -->
        <!--=============== scripts  ===============-->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugins.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>
    </body>

<!-- Mirrored from outdoor.kwst.net/site/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 14 Apr 2021 11:29:16 GMT -->
</html>