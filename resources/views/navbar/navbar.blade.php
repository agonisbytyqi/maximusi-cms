<!-- HEADER -->
<header id="header" class="header header-1 header_tran">
	<div id="top-bar" class="top-bar-section top-bar-bg-color">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-8">
					<div class="top_loction pull-left">
						<ul>
							<li>
								<a href="#!">
									<i class="fa fa-map-marker"></i> 123 Main Street, St. NW Ste </a>
							</li>
							<li>
								<a href="mailto:Support@Domain.Com">
									<i class="fa fa-envelope"></i> Support@Domain.Com</a>
							</li>
							<li>
								<a href="tel:1234567890">
									<i class="fa fa-phone"></i> +91 123 456 7890</a>
							</li>
						</ul>
					</div>
				</div>

				<div class="col-lg-4">
					<div class="top-social-icon icons-hover-black">
						<div class="icons-hover-black">
							<a href="javascript:avoid(0);">
								<i class="fa fa-facebook"></i>
							</a>
							<a href="javascript:avoid(0);">
								<i class="fa fa-twitter"></i>
							</a>
							<a href="javascript:avoid(0);">
								<i class="fa fa-youtube"></i>
							</a>
							<a href="javascript:avoid(0);">
								<i class="fa fa-linkedin"></i>
							</a>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	<div class="nav-wrap">
		<div class="reletiv_box">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-3">
						<div class="logo">
							<a href="/">
								<img src="/img/lika_red_transparent.svg" alt="" class="logo_top">
							</a>
						</div>
						<!-- Phone Menu button -->
						<button id="menu" class="menu hidden-md-up"></button>
					</div>
					<div class="col-md-9 nav-bg">
						<nav class="navigation">
							<ul>
								<?php if ($lang == "al") { ?>
								<li class="dropdown-nav">
									<a href="/" class="{{ (request()->is('home')) ? 'active' : '' }}">Ballina</a>
								</li>
								<li>
									<a href="/about" class="{{ Request::is('about') ? 'active' : '' }}">Rreth nesh</a>
								</li>
								<li class="dropdown-nav">
									<a href="/services"
										class="{{ (request()->is('services')) ? 'active' : '' }}">Serviset</a>
									<i class="ion-ios-plus-empty hidden-md-up"></i>
									<ul class="sub-nav">
										<li>
											<a href="/asphalt">Lika Asfalt</a>
										</li>
										<li>
											<a href="/concrete">Lika Beton</a>
										</li>
										<li>
											<a href="/stone">Gurthyes & Sertor</a>
										</li>
										<li>
											<a href="/construction">Lika Ndërtim i lartë</a>
										</li>
										<li>
											<a href="/low-construction">Lika Ndërtim i ulët</a>
										</li>
										<li>
											<a href="/summer-winter-maintenance">Mirëmbajtja verore dhe dimrore</a>
										</li>
									</ul>
								</li>
								<li>
									<a href="/projects"
										class="{{ (request()->is('projects')) ? 'active' : '' }}">Projektet</a>
								</li>
								<li>
									<a href="/career"
										class="{{ (request()->is('career')) ? 'active' : '' }}">Kariera</a>
								</li>
								<li>
									<a href="/contact"
										class="{{ (request()->is('contact')) ? 'active' : '' }}">Kontakti</a>
								</li>
								<?php } else { ?>
								<li class="dropdown-nav">
									<a href="/home/en/" class="{{ (request()->is('home/en')) ? 'active' : '' }}"">Home</a>
								</li>
								<li>
									<a href="/about/en/" class="{{ (request()->is('about/en')) ? 'active' : '' }}"">About</a>
								</li>
								<li class=" dropdown-nav">
									<a href="/services/en/"
										class="{{ (request()->is('services/en')) ? 'active' : '' }}">Services</a>
									<i class="ion-ios-plus-empty hidden-md-up"></i>
									<ul class="sub-nav">
										<li>
											<a href="/asphalt/en/">Lika Asphalt</a>
										</li>
										<li>
											<a href="/concrete/en/">Lika Concrete </a>
										</li>
										<li>
											<a href="/stone/en/">Breaking Stone</a>
										</li>
										<li>
											<a href="/construction/en/">High Construction</a>
										</li>
										<li>
											<a href="/low-construction/en/">Low Construction</a>
										</li>
										<li>
											<a href="/summer-winter-maintenance/en/">Summer and winter maintenance </a>
										</li>
									</ul>
								</li>
								<li>
									<a href="/projects/en/"
										class="{{ (request()->is('projects/en')) ? 'active' : '' }}">Project</a>
								</li>
								<li>
									<a href="/career/en/"
										class="{{ (request()->is('career/en')) ? 'active' : '' }}">Career</a>
								</li>
								<li>
									<a href="/contact/en/"
										class="{{ (request()->is('contact/en')) ? 'active' : '' }}">Contact Us</a>
								</li>
								<?php } ?>
								<li class="flamurat"><a href="/{{$slug}}/"><img src="/img/shqiptar.png" alt=""></a>
								</li>
								<li class="flamurat"><a href="/{{$slug}}/en/"><img src="/img/anglez (1).png" alt=""></a>
								</li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END HEADER -->