@extends('index')
@section('title_and_meta')


<div class="contact_header">
    @endsection
    @section('content')
</div>



<!-- CONTENT -->
<!-- Intro Section -->
<section class="inner-intro  padding bg-img overlay-dark light-color">
    <div class="container">
        <div class="row title">
            <h1>Project Details</h1>
            <div class="page-breadcrumb">
                <a>Home</a>/ <span>Project Details</span>
            </div>
        </div>
    </div>
</section>
<!-- End Intro Section -->
<!-- End Intro Section -->
<div id="project-detail-section" class="padding pb-60 pt-xs-60">
    <div class="container">
        <div class="row ">
            <div class="col-sm-12">
                <div class="heading-box pb-30 ">
                    <h2><span>Our</span> Project</h2>
                    <span class="b-line l-left"></span>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="project-details">
                    <figure>
                        <img src="/img/manjakos/{{ $entity[0]['collections']['main_image']}}" alt="">
                    </figure>
                    <div class="project-info col-sm-12 col-lg-4 ">
                        <h3>Project Description</h3>
                        <ul>
                            <?php if ($lang == "al") { ?>
                            <li>
                                <strong>Klienti:</strong> {{ $entity[0]['collections']['name'][$lang]}}
                            </li>
                            <li>
                                <strong>Lokacioni:</strong> {{ $entity[0]['collections']['location'][$lang]}}
                            </li>
                            <li>
                                <strong>Viti i projektit:</strong> {{ $entity[0]['collections']['year']}}
                            </li>
                            <li>
                                <strong>Vlera:</strong> {{ $entity[0]['collections']['value'][$lang]}}
                            </li>

                            <?php } else { ?>
                            <li>
                                <strong>Client:</strong> {{ $entity[0]['collections']['name'][$lang]}}
                            </li>
                            <li>
                                <strong>Location:</strong> {{ $entity[0]['collections']['location'][$lang]}}
                            </li>
                            <li>
                                <strong>Year Completed:</strong> {{ $entity[0]['collections']['year']}}
                            </li>
                            <li>
                                <strong>Value:</strong> {{ $entity[0]['collections']['value'][$lang]}}
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-7 mt-30">
                <div class="box-title mb-20">
                    {!! $entity[0]['collections']['description']["$lang"] !!}
                </div>
            </div>

            <div class="col-sm-12 col-lg-7 mt-40 mt-xs-15">
                <div class="row">
                </div>
            </div>
            <div class="col-sm-12 col-lg-12 mt-40">
                <ul class="project-gallery text-left">
                    @foreach( $entity[0]['collections']['gallery'] as $gallery)
                    <li>
                        <a href="{{URL::to('/img/manjakos/'. $gallery['image'])}}" class="fancylight"
                            data-fancybox-group="light">
                            <img src="{{URL::to('/img/manjakos/'. $gallery['image'])}}" alt="">
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>

        </div>
    </div>
</div>
<!-- Related Project-->

@include('includes.projects_slider')

@endsection