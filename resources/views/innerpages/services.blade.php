@extends('index')
@section('title_and_meta')


<div class="contact_header">
    @endsection
    @section('content')
</div>
<div class="member_header">
</div>

<!-- Start Bottom Header -->
<div class="page-area" style="background-image: url({{ URL::to('/img/manjakos/' . $entity[0]['collections']['image'])}})">
    <div class="breadcumb-overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="breadcrumb">
                    <div class="section-headline white-headline text-center">
                        <h3>Services Details</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END Header -->
<!-- End services Area -->
<div class="single-services-page area-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="page-head-left">
                    <!-- strat single area -->
                    <!-- <div class="single-page-head">
                        <div class="download-btn">
                            <h4>Download</h4>
                            <div class="about-btn">
                                <a href="#" class="down-btn">Download Brochure</a>
                                <a href="#" class="down-btn apli">Download Aplication</a>
                            </div>
                        </div>
                    </div> -->
                    <!-- strat single area -->
                    <div class="single-page-head">
                        <div class="clients-testi">
                            <div class="single-review text-center">
                                <div class="review-img ">
                                    <img src="img/review/1.jpg" alt="">
                                </div>
                                <div class="review-text">
                                    <p>When replacing a multi-lined selection of text, the generated dummy text maintains the amount of lines. When replacing a selection.</p>
                                    <h4>Arnold russel</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End left sidebar -->
            <!-- Start service page -->
            <div class="col-md-9 col-sm-8 col-xs-12">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="single-page">
                            <div class="page-img elec-page">
                                <img src="img/service/1.jpg" alt="">
                            </div>
                        </div>
                    </div>
                    <!-- strat single page -->
                    <!-- single-well end-->
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="single-well">
                            <a href="#">
                                <h3>{{ $entity[0]['collections']['name'][$lang]}}</h3>
                            </a>
                            <div class="desc">
                                {!! $entity[0]['collections']['details'][$lang] !!}
                            </div>
                            <div class="service_image_body" style="background-image: url({{ URL::to('/img/manjakos/' . $entity[0]['collections']['image'])}})">                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End page Area -->

@endsection
