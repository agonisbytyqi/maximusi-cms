<div id="services-section" class="padding ptb-xs-40">
    <div class="container-fluid">
        <div class="row text-center">
            <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 mb-30">

                <?php if ($lang == "al") { ?>
                <div class="heading-box">
                    <h2>Projektet Tona</h2>
                </div>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                    industry's standard dummy.
                </p>
                <?php } else { ?>
                <div class="heading-box">
                    <h2>Our Projects</h2>
                </div>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                    industry's standard dummy.
                </p>
                <?php } ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6 full-wid projektet_kryesore" ng-repeat="enets_saml in includes.global.many[0]['projects']">
                <div class="about-block img-scale mb-xs-40 clearfix border__b">
                    <figure>
                        <a href="/project/<%enets_saml.id%>/<%enets_saml.slugable.{{$lang}}%>">
                            <img class="img-responsive"
                                ng-src="/img/manjakos/<%enets_saml['collections']['main_image']%>">
                        </a>
                    </figure>
                    <div class="text-box mt-25">
                        <div class="box-title">
                            <h3>
                                <a href="#"><%enets_saml['collections']['name']["{{$lang}}"]%> </a>
                            </h3>
                        </div>
                        <div class="text-content">
                            <p ng-bind-html="enets_saml['collections']['description']['{{$lang}}']"></p>
                            <a href="/project/<%enets_saml.id%>/<%enets_saml.slugable.{{$lang}}%>" class="read__more">
                                <i class="ion-ios-arrow-thin-right"></i>
                            </a>
                        </div>
                    </div>
                 </div>

            </div>
        </div>
    </div>
</div>