
<div class="padding gray-bg">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="heading-box pb-30 ">
                    <h2><span>Related</span> Project</h2>
                    <span class="b-line l-left"></span>
                </div>

            </div>
            <div class="col-sm-12">
                <div id="related-project" class="nf-carousel-theme">
                    <div class="project-item slider_proejcts" ng-repeat="enets_saml in includes.global.many[0]['projects']">
                        <div class="about-block clearfix">
                            <figure>
                                <a href="/project/<%enets_saml.id%>/<%enets_saml.slugable.{{$lang}}%>"><img class="img-responsive"
                                ng-src="/img/manjakos/<%enets_saml['collections']['main_image']%>"></a>
                            </figure>
                            <div class="text-box mt-25">
                                <div class="box-title mb-15">
                                    <h3><a href="/project/<%enets_saml.id%>/<%enets_saml.slugable.{{$lang}}%>"> <%enets_saml['collections']['name']["{{$lang}}"]%></a></h3>
                                </div>
                                <div class="text-content">
                                <p ng-bind-html="enets_saml['collections']['description']['{{$lang}}']"></p>
                                    <a href="/project/<%enets_saml.id%>/<%enets_saml.slugable.{{$lang}}%>" class="btn-text mt-15">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>