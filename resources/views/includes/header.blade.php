<header>
                <!-- Header inner  -->
                <div class="header-inner">
                    <!-- Logo  -->
                    <div class="logo-holder">
                        <a href="home"><img src="img/logo.png" alt=""></a>
                    </div>
                    <!--Logo end  -->
                    <!--Navigation  -->
                    <div class="nav-button-holder">
                        <div class="nav-button vis-m"><span></span><span></span><span></span></div>
                    </div>
                    <div class="show-share isShare">Share</div>
                    <div class="nav-holder">
                        <nav>
                            <ul>
                                <li><a href="home" class="act-link">Home</a></li>
                                <li><a href="about">About us </a>
                                <li><a href="portfolio">Portfolio</a></li>
                                <li><a href="contact">Contact</a></li>
                                
                                
                                     
                        </nav>
                    </div>
                    <!--navigation end -->
                </div>
                <!--Header inner end  -->
            </header>