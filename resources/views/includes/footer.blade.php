<footer>
                <div class="policy-box">
                    <span>&#169; Outdoor 2015 . All rights reserved. </span>
                    <ul>
                        <li><a href="#">yourmail@domain.com</a></li>
                        <li><a href="#">+7(111)123456789</a></li>
                    </ul>
                </div>
                <!-- footer social -->
                <div class="footer-social">
                    <ul>
                        <li><a href="#" target="_blank" ><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#" target="_blank" ><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#" target="_blank" ><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="#" target="_blank" ><i class="fa fa-tumblr"></i></a></li>
                    </ul>
                </div>
                <!-- footer social end -->
                <div class="to-top"><i class="fa fa-angle-up"></i></div>
            </footer>
	<!-- END FOOTER -->