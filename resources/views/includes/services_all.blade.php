<div class="services-area services-4 area-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="section-headline text-center">
                    <h3><%includes.global.one[0]['services_sub']['collections']['title']["{{$lang}}"]%></h3>
                    <p ng-bind-html="includes.global.one[0]['services_sub']['collections']['description']['{{$lang}}']"></p>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <!-- Start services -->
            <div class="col-md-4 col-sm-6 col-xs-12" ng-repeat="enets_saml in includes.global.many[1]['services']">
                <div class="single-service">
                    <!-- <span class="slugan">25% <br />off</span> -->
                    <a class="service-image" href="#">
                        <img ng-src="/img/manjakos/<%enets_saml['collections']['image']%>">
                    </a>
                    <div class="service-content">
                        <h4><%enets_saml['collections']['name']["{{$lang}}"]%></h4>
                        <p ng-bind-html="enets_saml['collections']['details']['{{$lang}}']"></p>
                        <?php if ($lang == "en") { ?>
                            <a class="service-btn" href="/services/<%enets_saml.id%>/<%enets_saml.slugable.{{$lang}}%>/{{$lang}}">read more</a>
                        <?php } else { ?>
                            <a class="service-btn" href="/services/<%enets_saml.id%>/<%enets_saml.slugable.{{$lang}}%>/{{$lang}}">më shumë</a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>