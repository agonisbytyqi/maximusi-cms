-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 17, 2021 at 01:59 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `likatrade`
--

-- --------------------------------------------------------

--
-- Table structure for table `bld_building`
--

CREATE TABLE `bld_building` (
  `id` int(10) UNSIGNED NOT NULL,
  `reference_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sub_location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `commision` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cheques` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bedrooms` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bathrooms` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parking` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `map_frame` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sales_rent` enum('sales','rent') COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `favourite` tinyint(1) NOT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci,
  `price` double UNSIGNED DEFAULT '0',
  `area` double UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bld_gallery`
--

CREATE TABLE `bld_gallery` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `building_id` int(10) UNSIGNED NOT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bld_relation`
--

CREATE TABLE `bld_relation` (
  `id` int(10) UNSIGNED NOT NULL,
  `building_id` int(10) UNSIGNED NOT NULL,
  `relation_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_gallery_entity`
--

CREATE TABLE `cms_gallery_entity` (
  `id` int(10) UNSIGNED NOT NULL,
  `field_id` int(10) UNSIGNED NOT NULL,
  `entity_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cms_gallery_entity`
--

INSERT INTO `cms_gallery_entity` (`id`, `field_id`, `entity_id`, `created_at`, `updated_at`) VALUES
(1, 11, 3, '2021-02-20 16:37:17', '2021-02-20 16:37:17'),
(3, 11, 6, '2021-02-22 12:39:37', '2021-02-22 12:39:37'),
(4, 45, 26, '2021-03-06 10:04:56', '2021-03-06 10:04:56'),
(5, 51, 28, '2021-03-06 10:39:36', '2021-03-06 10:39:36'),
(6, 57, 30, '2021-03-06 10:45:21', '2021-03-06 10:45:21'),
(7, 65, 32, '2021-03-06 11:25:48', '2021-03-06 11:25:48'),
(8, 72, 34, '2021-03-06 11:33:39', '2021-03-06 11:33:39'),
(9, 79, 36, '2021-03-06 11:47:18', '2021-03-06 11:47:18');

-- --------------------------------------------------------

--
-- Table structure for table `cms_gallery_images`
--

CREATE TABLE `cms_gallery_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `gallery_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cms_gallery_images`
--

INSERT INTO `cms_gallery_images` (`id`, `gallery_id`, `image`, `created_at`, `updated_at`, `order`) VALUES
(17, 7, '06108943e0e0dd4c92da79e32f84e4aa6.jpg', '2021-03-06 11:26:17', '2021-03-06 11:26:17', 1),
(18, 7, '16108943e0e0dd4c92da79e32f84e4aa6.jpg', '2021-03-06 11:26:17', '2021-03-06 11:26:17', 2),
(19, 7, '26108943e0e0dd4c92da79e32f84e4aa6.jpg', '2021-03-06 11:26:17', '2021-03-06 11:26:17', 3),
(20, 8, '0c8dcc1279ab4d99d4b0f6fb20da76618.jpg', '2021-03-06 11:34:12', '2021-03-06 11:34:12', 1),
(21, 8, '1c8dcc1279ab4d99d4b0f6fb20da76618.jpg', '2021-03-06 11:34:12', '2021-03-06 11:34:12', 2),
(22, 8, '2c8dcc1279ab4d99d4b0f6fb20da76618.jpg', '2021-03-06 11:34:12', '2021-03-06 11:34:12', 3),
(23, 9, '0c4b49f2771c0eb0fc40a5f32ff1bbd7b.jpg', '2021-03-06 11:47:45', '2021-03-06 11:47:45', 1),
(24, 9, '1c4b49f2771c0eb0fc40a5f32ff1bbd7b.jpg', '2021-03-06 11:47:45', '2021-03-06 11:47:45', 2),
(25, 9, '2c4b49f2771c0eb0fc40a5f32ff1bbd7b.jpg', '2021-03-06 11:47:45', '2021-03-06 11:47:45', 3),
(35, 4, '01a7969e6e702c9bf1248216aabee4813.jpeg', '2021-03-17 02:31:59', '2021-03-17 02:31:59', 1),
(36, 4, '11a7969e6e702c9bf1248216aabee4813.jpeg', '2021-03-17 02:31:59', '2021-03-17 02:31:59', 2),
(37, 4, '21a7969e6e702c9bf1248216aabee4813.jpeg', '2021-03-17 02:31:59', '2021-03-17 02:31:59', 3),
(38, 4, '31a7969e6e702c9bf1248216aabee4813.jpeg', '2021-03-17 02:31:59', '2021-03-17 02:31:59', 4),
(39, 4, '41a7969e6e702c9bf1248216aabee4813.jpeg', '2021-03-17 02:31:59', '2021-03-17 02:31:59', 5),
(40, 4, '0ab2a11cf1c786b3c379bdd6df4d36a90.jpeg', '2021-03-17 02:32:52', '2021-03-17 02:32:52', 6),
(41, 4, '1ab2a11cf1c786b3c379bdd6df4d36a90.jpeg', '2021-03-17 02:32:52', '2021-03-17 02:32:52', 7),
(42, 4, '2ab2a11cf1c786b3c379bdd6df4d36a90.jpeg', '2021-03-17 02:32:52', '2021-03-17 02:32:52', 8),
(43, 4, '3ab2a11cf1c786b3c379bdd6df4d36a90.jpeg', '2021-03-17 02:32:52', '2021-03-17 02:32:52', 9),
(44, 4, '076187575c90de8a8a2ee2163aa6985ab.jpeg', '2021-03-17 02:35:36', '2021-03-17 02:35:36', 10),
(45, 4, '176187575c90de8a8a2ee2163aa6985ab.jpeg', '2021-03-17 02:35:36', '2021-03-17 02:35:36', 11),
(46, 4, '276187575c90de8a8a2ee2163aa6985ab.jpeg', '2021-03-17 02:35:36', '2021-03-17 02:35:36', 12),
(47, 5, '045a475eb076a0cd90a37bb4da010a7a9.jpeg', '2021-03-17 03:25:16', '2021-03-17 03:25:16', 1),
(48, 5, '145a475eb076a0cd90a37bb4da010a7a9.jpeg', '2021-03-17 03:25:16', '2021-03-17 03:25:16', 2),
(49, 5, '245a475eb076a0cd90a37bb4da010a7a9.jpeg', '2021-03-17 03:25:16', '2021-03-17 03:25:16', 3),
(50, 5, '345a475eb076a0cd90a37bb4da010a7a9.jpeg', '2021-03-17 03:25:16', '2021-03-17 03:25:16', 4),
(51, 5, '445a475eb076a0cd90a37bb4da010a7a9.jpeg', '2021-03-17 03:25:16', '2021-03-17 03:25:16', 5),
(52, 6, '06ec85ba102281ef0cafd65456c520802.jpg', '2021-03-17 03:27:46', '2021-03-17 03:27:46', 1),
(53, 3, '0f5dd8d8c805edaf17b60c55a078ff687111615934548.jpeg', '2021-03-17 03:42:28', '2021-03-17 03:42:28', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cms_languages`
--

CREATE TABLE `cms_languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cms_languages`
--

INSERT INTO `cms_languages` (`id`, `key`, `name`, `created_at`, `updated_at`, `order`) VALUES
(1, 'en', 'English', '2021-01-19 20:59:15', '2021-01-19 20:59:16', 1),
(2, 'al', 'Albanian', '2021-01-19 20:59:15', '2021-01-19 20:59:16', 2);

-- --------------------------------------------------------

--
-- Table structure for table `cms_pages`
--

CREATE TABLE `cms_pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cms_pages`
--

INSERT INTO `cms_pages` (`id`, `parent_id`, `lft`, `rgt`, `depth`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 2, 0, '{\"en\":\"Home\"}', 'home', '2021-01-19 21:00:24', '2021-01-19 21:00:24'),
(2, NULL, 3, 4, 0, '{\"en\":\"About Us\"}', 'about', '2021-01-28 13:07:54', '2021-01-28 13:07:54'),
(3, NULL, 5, 6, 0, '{\"en\":\"Services\"}', 'services', '2021-01-28 13:41:02', '2021-01-28 13:41:02'),
(4, NULL, 7, 8, 0, '{\"en\":\"Projects\"}', 'projects', '2021-01-28 13:55:33', '2021-01-28 13:55:33'),
(5, NULL, 9, 10, 0, '{\"en\":\"Career\"}', 'career', '2021-01-28 14:03:36', '2021-01-28 14:03:36'),
(6, NULL, 11, 12, 0, '{\"en\":\"Contact\"}', 'contact', '2021-01-28 14:03:49', '2021-01-28 14:03:49'),
(7, NULL, 13, 14, 0, '{\"en\":\"Construction\"}', 'construction', '2021-02-22 13:28:29', '2021-02-22 13:28:29'),
(8, NULL, 15, 16, 0, '{\"en\":\"Low Construction\"}', 'low-construction', '2021-03-06 08:31:44', '2021-03-06 08:31:44'),
(9, NULL, 17, 18, 0, '{\"en\":\"Asphalt\"}', 'asphalt', '2021-03-06 08:31:55', '2021-03-06 08:31:55'),
(10, NULL, 19, 20, 0, '{\"en\":\"Beton\"}', 'concrete', '2021-03-06 08:32:11', '2021-03-06 08:32:11'),
(11, NULL, 21, 22, 0, '{\"en\":\"Stone\"}', 'stone', '2021-03-06 08:32:23', '2021-03-06 08:32:23'),
(12, NULL, 23, 24, 0, '{\"en\":\"Summer\\/Winter\"}', 'summer-winter-maintenance', '2021-03-06 08:33:05', '2021-03-06 08:33:05');

-- --------------------------------------------------------

--
-- Table structure for table `cms_pages_collections`
--

CREATE TABLE `cms_pages_collections` (
  `id` int(10) UNSIGNED NOT NULL,
  `entity_id` int(10) UNSIGNED NOT NULL,
  `field_id` int(10) UNSIGNED NOT NULL,
  `field_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `field_type` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `value` mediumtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cms_pages_collections`
--

INSERT INTO `cms_pages_collections` (`id`, `entity_id`, `field_id`, `field_name`, `field_type`, `value`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'title', 'text', '{\"al\":\"LIKA ASFALLT\",\"en\":\"Lika Asphalt\"}', '2021-02-01 13:35:55', '2021-03-16 18:35:09'),
(2, 1, 2, 'description', 'editor', '{\"al\":\"<p>B&euml;jm prodhimin e t&euml; gjitha lloj&euml;ve t&euml; asfalltit t&euml; gatsh&euml;m sipas standart&euml;ve<\\/p>\",\"en\":\"<p>We make all types of asphalt ready according to standards<\\/p>\"}', '2021-02-01 13:35:55', '2021-03-17 00:31:18'),
(13, 3, 5, 'name', 'text', '{\"al\":\"\\u201cZgjerimi i rrug\\u00ebs nacionale N 9.1, segmenti Dollc-Gjakov\\u00eb , L=31.00 \\u201d\",\"en\":\"\\u201cZgjerimi i rrug\\u00ebs nacionale N 9.1, segmenti Dollc-Gjakov\\u00eb , L=31.00 \\u201d\"}', '2021-02-20 16:37:17', '2021-03-17 03:47:55'),
(14, 3, 5, 'name', 'text', '{\"al\":\"\\u201cZgjerimi i rrug\\u00ebs nacionale N 9.1, segmenti Dollc-Gjakov\\u00eb , L=31.00 \\u201d\",\"en\":\"\\u201cZgjerimi i rrug\\u00ebs nacionale N 9.1, segmenti Dollc-Gjakov\\u00eb , L=31.00 \\u201d\"}', '2021-02-20 16:37:17', '2021-03-17 03:47:55'),
(15, 3, 6, 'client', 'text', '{\"al\":\"Gjakov\\u00eb\",\"en\":\"-Gjakov\\u00eb\"}', '2021-02-20 16:37:17', '2021-03-17 03:47:55'),
(16, 3, 7, 'location', 'text', '{\"al\":\"Prishtine\",\"en\":\"Gjakov\\u00eb\"}', '2021-02-20 16:37:17', '2021-03-17 03:47:55'),
(17, 3, 8, 'year', 'date', '\"Wed Feb 10 2021 18:37:00 GMT+0100 (Central European Standard Time)\"', '2021-02-20 16:37:17', '2021-02-20 16:37:40'),
(18, 3, 9, 'value', 'text', '{\"al\":\"\",\"en\":\"\"}', '2021-02-20 16:37:17', '2021-03-17 03:47:55'),
(19, 3, 10, 'description', 'editor', '{\"al\":\"\",\"en\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<\\/p>\"}', '2021-02-20 16:37:17', '2021-03-17 03:45:57'),
(20, 3, 11, 'gallery', 'gallery', NULL, '2021-02-20 16:37:17', '2021-03-17 03:45:57'),
(21, 3, 12, 'main_image', 'file', '\"211615934757.jpeg\"', '2021-02-20 16:37:17', '2021-03-17 03:45:57'),
(39, 6, 5, 'name', 'text', '{\"al\":\"Nd\\u00ebrtimi - Zgjerimi i rrug\\u00ebs R 107 q\\u00ebndrore n\\u00eb De\\u00e7an\",\"en\":\"Nd\\u00ebrtimi - Zgjerimi i rrug\\u00ebs R 107 q\\u00ebndrore n\\u00eb De\\u00e7an\"}', '2021-02-22 12:39:37', '2021-03-17 03:49:36'),
(40, 6, 5, 'name', 'text', '{\"al\":\"Nd\\u00ebrtimi - Zgjerimi i rrug\\u00ebs R 107 q\\u00ebndrore n\\u00eb De\\u00e7an\",\"en\":\"Nd\\u00ebrtimi - Zgjerimi i rrug\\u00ebs R 107 q\\u00ebndrore n\\u00eb De\\u00e7an\"}', '2021-02-22 12:39:37', '2021-03-17 03:49:36'),
(41, 6, 6, 'client', 'text', '{\"al\":\"Komuna e De\\u00e7anit\",\"en\":\"De\\u00e7an Municipality\"}', '2021-02-22 12:39:37', '2021-03-17 03:31:55'),
(42, 6, 7, 'location', 'text', '{\"al\":\"DE\\u00c7AN\",\"en\":\"De\\u00e7an Municipality\"}', '2021-02-22 12:39:37', '2021-03-17 03:49:58'),
(43, 6, 8, 'year', 'date', '\"Thu Feb 18 2021 14:39:00 GMT+0100 (Central European Standard Time)\"', '2021-02-22 12:39:37', '2021-02-22 12:40:48'),
(44, 6, 9, 'value', 'text', '{\"al\":\"\",\"en\":\"\"}', '2021-02-22 12:39:37', '2021-03-17 03:50:13'),
(45, 6, 10, 'description', 'editor', '{\"al\":\"\",\"en\":\"<p>Another one<\\/p>\"}', '2021-02-22 12:39:37', '2021-03-17 03:39:21'),
(46, 6, 11, 'gallery', 'gallery', '[{\"id\":\"53\"},{\"gallery_id\":\"3\"},{\"image\":\"0f5dd8d8c805edaf17b60c55a078ff687111615934548.jpeg\"},{\"created_at\":\"2021-03-16 22:42:28\"},{\"updated_at\":\"2021-03-16 22:42:28\"},{\"order\":\"1\"}]', '2021-02-22 12:39:37', '2021-03-17 03:42:46'),
(47, 6, 12, 'main_image', 'file', '\"471615934548.jpeg\"', '2021-02-22 12:39:37', '2021-03-17 03:42:28'),
(48, 1, 13, 'title2', 'text', '{\"al\":\"LIKA BETON\",\"en\":\"LIKA BETON\"}', '2021-02-22 13:37:00', '2021-03-17 00:36:00'),
(49, 1, 14, 'description2', 'editor', '{\"al\":\"<p>Fabrika p&euml;r prodhimin e betonit.<\\/p>\",\"en\":\"<p>&nbsp;Factory for the production of concrete.&nbsp;<\\/p>\"}', '2021-02-22 13:37:13', '2021-03-17 00:43:58'),
(50, 1, 15, 'title3', 'text', '{\"al\":\"GURTHYES & SERTOR\",\"en\":\"BREAKING STONE\"}', '2021-02-22 13:37:24', '2021-03-17 00:47:13'),
(51, 1, 16, 'description3', 'editor', '{\"al\":\"<p>Prodhojm&euml; t&euml; gjitha fraksion&euml;t e gurit t&euml; thyer p&euml;r nd&euml;rtim t&euml; ul&euml;t dhe t&euml; lart&euml;.<\\/p>\",\"en\":\"<p>We produce all broken stone fractions for low and high construction.<\\/p>\"}', '2021-02-22 13:37:45', '2021-03-17 00:47:13'),
(52, 7, 17, 'title', 'text', '{\"al\":\"Ne ofrojm\\u00eb sh\\u00ebrbime t\\u00eb personalizuara dhe me cil\\u00ebsi t\\u00eb lart\\u00eb\",\"en\":\"WE PROVIDING PERSONALIZED AND HIGH QUALITY SERVICES\"}', '2021-02-22 13:55:10', '2021-02-22 13:55:32'),
(53, 7, 18, 'description', 'editor', '{\"al\":\"<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s.<\\/p>\",\"en\":\"<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<\\/p>\"}', '2021-02-22 13:55:10', '2021-03-15 12:14:14'),
(54, 8, 19, 'name', 'text', '{\"al\":\"Joe Doe 2\",\"en\":\"Joe Doe\"}', '2021-02-22 13:59:30', '2021-02-23 10:26:15'),
(55, 8, 19, 'name', 'text', '{\"al\":\"Joe Doe 2\",\"en\":\"Joe Doe\"}', '2021-02-22 13:59:30', '2021-02-23 10:26:15'),
(56, 8, 20, 'job_position', 'text', '{\"al\":\"Ceo\",\"en\":\"CEO\"}', '2021-02-22 13:59:30', '2021-02-23 10:24:09'),
(57, 8, 21, 'description', 'editor', '{\"al\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod [..]<\\/p>\",\"en\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod [..]<\\/p>\"}', '2021-02-22 13:59:30', '2021-02-23 10:24:09'),
(58, 8, 22, 'image', 'file', '\"221614005970.jpg\"', '2021-02-22 13:59:30', '2021-02-22 13:59:30'),
(59, 9, 23, 'title', 'text', '{\"al\":\"Cientific Results Conference Eduma Summer 2017.\",\"en\":\"Cientific Results Conference Eduma Summer 2017.\"}', '2021-02-23 10:30:30', '2021-02-23 10:30:30'),
(60, 9, 23, 'title', 'text', '{\"al\":\"Cientific Results Conference Eduma Summer 2017.\",\"en\":\"Cientific Results Conference Eduma Summer 2017.\"}', '2021-02-23 10:30:30', '2021-02-23 10:30:30'),
(61, 9, 24, 'date', 'text', '{\"al\":\"25 Qeshor\",\"en\":\"25 June\"}', '2021-02-23 10:30:30', '2021-02-23 10:30:30'),
(62, 9, 25, 'author', 'text', '{\"al\":\"Jon Doe\",\"en\":\"Jon Doe\"}', '2021-02-23 10:30:30', '2021-02-23 10:30:30'),
(63, 9, 26, 'image', 'file', '\"261614079830.jpg\"', '2021-02-23 10:30:30', '2021-02-23 10:30:30'),
(64, 9, 27, 'link', 'text', '{\"al\":\"https:\\/\\/facebook.com\",\"en\":\"https:\\/\\/facebook.com\"}', '2021-02-23 10:30:30', '2021-02-23 10:30:35'),
(65, 10, 28, 'title', 'text', '{\"al\":\"SELIA E KOMPANIS\\u00cb\",\"en\":\"COMPANY HEADQUARTERS\"}', '2021-02-23 12:41:49', '2021-03-17 03:57:34'),
(66, 10, 29, 'description', 'editor', '{\"al\":\"<p>B&euml;jm prodhimin e t&euml; gjitha lloj&euml;ve t&euml; asfalltit t&euml; gatsh&euml;m sipas standart&euml;ve, si dhe prodhojm&euml; t&euml; gjitha fraksion&euml;t e gurit t&euml; thyer p&euml;r nd&euml;rtim t&euml; ul&euml;t dhe t&euml; lart&euml;.<\\/p>\\n\\n<p>B&euml;jm&euml; nd&euml;rtimin e t&euml; gjitha kategorive t&euml; rrug&euml;ve me infrastruktur&euml; p&euml;rcjell&euml;se, si dhe nd&euml;rtim t&euml; lart&euml;.<\\/p>\",\"en\":\"<p>Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt.<\\/p>\"}', '2021-02-23 12:41:49', '2021-03-17 03:55:09'),
(67, 10, 30, 'image', 'file', '\"671615935309.jpeg\"', '2021-02-23 12:41:49', '2021-03-17 03:55:09'),
(68, 10, 31, 'title2', 'text', '{\"al\":\"Vizioni jon\\u00eb!\",\"en\":\"Our Vision\"}', '2021-02-23 12:41:49', '2021-02-23 12:42:02'),
(69, 10, 32, 'description2', 'editor', '{\"al\":\"<p>Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt.<\\/p>\",\"en\":\"<p>Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt.<\\/p>\"}', '2021-02-23 12:41:49', '2021-02-23 12:41:49'),
(70, 10, 33, 'image2', 'file', '\"701615935309.jpeg\"', '2021-02-23 12:41:49', '2021-03-17 03:55:09'),
(71, 11, 34, 'name', 'text', '{\"al\":\"?\",\"en\":\"?\"}', '2021-02-23 12:50:56', '2021-03-17 04:04:34'),
(72, 11, 35, 'job_position', 'text', '{\"al\":\"CEO\",\"en\":\"CEO Letter\"}', '2021-02-23 12:50:56', '2021-03-17 04:04:34'),
(73, 11, 36, 'paragraph', 'editor', '{\"al\":\"<p>Welcome to the Lika Trade website, the infrastructure area of the FCC Group, a world leader in environmental, water management and infrastructure services. For 120 years we have been building cities for living, working as experts in civil engineering, building, rehabilitation, industrial construction and concessions to offer the public life-improving infrastructure &ndash; roads, railway lines, airports, water and maritime works, tunnels and bridges as well as projects for residential and non-residential buildings such as hospitals, sports stadiums, museums and offices. My appointment occurs at a time of renovation while our identity remains thanks to the thousands of professionals who form part of the company. This great team is our main strength and creates enormous technical and human capacity development value in our projects. We are starting an era in which we wish to recover a leading position in world infrastructure, nationally and internationally, with a view to the future. We want to increasingly offer the best solutions to clients because in Lika Trade, our ambition is to continue to build a better world for everyone. We are a sustainable company that has a clear and firm commitment to dialogue and transparency with our stakeholders. To achieve this we are in a full digital transformation process, improving and renovating our channels to provide visitors to our website with the latest company news.<\\/p>\",\"en\":\"<p>Welcome to the Lika Trade website, the infrastructure area of the FCC Group, a world leader in environmental, water management and infrastructure services. For 120 years we have been building cities for living, working as experts in civil engineering, building, rehabilitation, industrial construction and concessions to offer the public life-improving infrastructure &ndash; roads, railway lines, airports, water and maritime works, tunnels and bridges as well as projects for residential and non-residential buildings such as hospitals, sports stadiums, museums and offices. My appointment occurs at a time of renovation while our identity remains thanks to the thousands of professionals who form part of the company. This great team is our main strength and creates enormous technical and human capacity development value in our projects. We are starting an era in which we wish to recover a leading position in world infrastructure, nationally and internationally, with a view to the future. We want to increasingly offer the best solutions to clients because in Lika Trade, our ambition is to continue to build a better world for everyone. We are a sustainable company that has a clear and firm commitment to dialogue and transparency with our stakeholders. To achieve this we are in a full digital transformation process, improving and renovating our channels to provide visitors to our website with the latest company news.<\\/p>\"}', '2021-02-23 12:50:56', '2021-02-23 12:56:05'),
(74, 11, 37, 'image', 'file', '\"741615936134.png\"', '2021-02-23 12:50:56', '2021-03-17 04:08:54'),
(75, 12, 38, 'name', 'text', '{\"al\":\"\",\"en\":\"1\"}', '2021-02-23 12:57:12', '2021-02-23 12:57:12'),
(76, 12, 38, 'name', 'text', '{\"al\":\"\",\"en\":\"1\"}', '2021-02-23 12:57:12', '2021-02-23 12:57:12'),
(77, 12, 39, 'logo', 'file', '\"391614088632.png\"', '2021-02-23 12:57:12', '2021-02-23 12:57:12'),
(78, 13, 38, 'name', 'text', '{\"al\":\"\",\"en\":\"2\"}', '2021-02-23 12:57:21', '2021-02-23 12:57:21'),
(79, 13, 38, 'name', 'text', '{\"al\":\"\",\"en\":\"2\"}', '2021-02-23 12:57:21', '2021-02-23 12:57:21'),
(80, 13, 39, 'logo', 'file', '\"391614088641.png\"', '2021-02-23 12:57:21', '2021-02-23 12:57:21'),
(81, 14, 38, 'name', 'text', '{\"al\":\"\",\"en\":\"3\"}', '2021-02-23 12:57:31', '2021-02-23 12:57:31'),
(82, 14, 38, 'name', 'text', '{\"al\":\"\",\"en\":\"3\"}', '2021-02-23 12:57:31', '2021-02-23 12:57:31'),
(83, 14, 39, 'logo', 'file', '\"391614088651.png\"', '2021-02-23 12:57:31', '2021-02-23 12:57:31'),
(84, 15, 38, 'name', 'text', '{\"al\":\"\",\"en\":\"4\"}', '2021-02-23 12:57:39', '2021-02-23 12:57:39'),
(85, 15, 38, 'name', 'text', '{\"al\":\"\",\"en\":\"4\"}', '2021-02-23 12:57:39', '2021-02-23 12:57:39'),
(86, 15, 39, 'logo', 'file', '\"391614088659.png\"', '2021-02-23 12:57:39', '2021-02-23 12:57:39'),
(87, 16, 38, 'name', 'text', '{\"al\":\"\",\"en\":\"5\"}', '2021-02-23 12:57:48', '2021-02-23 12:57:48'),
(88, 16, 38, 'name', 'text', '{\"al\":\"\",\"en\":\"5\"}', '2021-02-23 12:57:48', '2021-02-23 12:57:48'),
(89, 16, 39, 'logo', 'file', '\"391614088669.png\"', '2021-02-23 12:57:49', '2021-02-23 12:57:49'),
(90, 17, 38, 'name', 'text', '{\"al\":\"\",\"en\":\"6\"}', '2021-02-23 12:57:56', '2021-02-23 12:57:56'),
(91, 17, 38, 'name', 'text', '{\"al\":\"\",\"en\":\"6\"}', '2021-02-23 12:57:56', '2021-02-23 12:57:56'),
(92, 17, 39, 'logo', 'file', '\"391614088676.png\"', '2021-02-23 12:57:56', '2021-02-23 12:57:56'),
(93, 18, 38, 'name', 'text', '{\"al\":\"\",\"en\":\"7\"}', '2021-02-23 12:58:05', '2021-02-23 12:58:05'),
(94, 18, 38, 'name', 'text', '{\"al\":\"\",\"en\":\"7\"}', '2021-02-23 12:58:05', '2021-02-23 12:58:05'),
(95, 18, 39, 'logo', 'file', '\"391614088685.png\"', '2021-02-23 12:58:05', '2021-02-23 12:58:05'),
(96, 19, 38, 'name', 'text', '{\"al\":\"\",\"en\":\"8\"}', '2021-02-23 13:01:34', '2021-02-23 13:01:34'),
(97, 19, 38, 'name', 'text', '{\"al\":\"\",\"en\":\"8\"}', '2021-02-23 13:01:34', '2021-02-23 13:01:34'),
(98, 19, 39, 'logo', 'file', '\"391614088894.png\"', '2021-02-23 13:01:34', '2021-02-23 13:01:34'),
(99, 20, 38, 'name', 'text', '{\"al\":\"\",\"en\":\"9\"}', '2021-02-23 13:01:42', '2021-02-23 13:01:42'),
(100, 20, 38, 'name', 'text', '{\"al\":\"\",\"en\":\"9\"}', '2021-02-23 13:01:42', '2021-02-23 13:01:42'),
(101, 20, 39, 'logo', 'file', '\"391614088902.png\"', '2021-02-23 13:01:42', '2021-02-23 13:01:42'),
(102, 21, 38, 'name', 'text', '{\"al\":\"\",\"en\":\"10\"}', '2021-02-23 13:02:00', '2021-02-23 13:02:00'),
(103, 21, 38, 'name', 'text', '{\"al\":\"\",\"en\":\"10\"}', '2021-02-23 13:02:00', '2021-02-23 13:02:00'),
(104, 21, 39, 'logo', 'file', '\"391614088920.png\"', '2021-02-23 13:02:00', '2021-02-23 13:02:00'),
(105, 22, 40, 'title', 'text', '{\"al\":\"Kontabilist\",\"en\":\"Accountant\"}', '2021-02-23 13:18:10', '2021-02-23 13:18:59'),
(106, 22, 40, 'title', 'text', '{\"al\":\"Kontabilist\",\"en\":\"Accountant\"}', '2021-02-23 13:18:10', '2021-02-23 13:18:59'),
(107, 23, 40, 'title', 'text', '{\"al\":\"Arkitekt\",\"en\":\"Architect\"}', '2021-02-23 13:18:25', '2021-02-23 13:18:38'),
(108, 23, 40, 'title', 'text', '{\"al\":\"Arkitekt\",\"en\":\"Architect\"}', '2021-02-23 13:18:25', '2021-02-23 13:18:38'),
(109, 24, 40, 'title', 'text', '{\"al\":\"Inxhinier\",\"en\":\"Engineer\"}', '2021-02-23 13:19:13', '2021-02-23 13:19:13'),
(110, 24, 40, 'title', 'text', '{\"al\":\"Inxhinier\",\"en\":\"Engineer\"}', '2021-02-23 13:19:13', '2021-02-23 13:19:13'),
(111, 25, 41, 'title', 'text', '{\"al\":\"Nd\\u00ebrtim i lart\\u00eb\",\"en\":\"High Construction\"}', '2021-03-06 09:57:13', '2021-03-06 09:57:13'),
(112, 25, 42, 'background', 'file', '\"1121615900353.jpeg\"', '2021-03-06 09:57:13', '2021-03-16 18:12:33'),
(113, 26, 43, 'image', 'file', '\"1131615930319.jpeg\"', '2021-03-06 10:04:56', '2021-03-17 02:31:59'),
(114, 26, 44, 'description', 'editor', '{\"al\":\"<p>Lika Construction synon t&euml; jet&euml; lider i realizimit t&euml; projekteve t&euml; m&euml;dha nd&euml;rtimore dhe nd&euml;rtimit modern t&euml; &nbsp;objekteve t&euml; larta banesore n&euml; Kosov&euml;, me gjith&euml; infrastruktur&euml;n p&euml;rcjell&euml;se. Lika Construction p&euml;rfshin organizimin, pun&euml;n profesionale dhe cil&euml;sin&euml; me standarde t&euml; larta nd&euml;rtimi n&euml; p&euml;rputhje me k&euml;rkesat dhe kriteret e nd&euml;rtimit bashk&euml;kohor, duke pasur gjithmon&euml; prioritet plot&euml;simin e k&euml;rkesave dhe d&euml;shirave t&euml; klient&euml;ve tan&euml;.<\\/p>\",\"en\":\"<p>Lika Construction aims to be a leader in the implementation of large construction projects and modern construction of high-rise residential buildings in Kosovo, with all the accompanying infrastructure. Lika Construction includes organization, professional work and quality with high construction standards in accordance with the requirements and criteria of contemporary construction, always having priority meeting the requirements and wishes of our customers.<\\/p>\"}', '2021-03-06 10:04:56', '2021-03-17 02:34:06'),
(115, 26, 45, 'gallery', 'gallery', '[{\"id\":\"43\"},{\"gallery_id\":\"4\"},{\"image\":\"3ab2a11cf1c786b3c379bdd6df4d36a90.jpeg\"},{\"created_at\":\"2021-03-16 21:32:52\"},{\"updated_at\":\"2021-03-16 21:32:52\"},{\"order\":\"9\"},{\"id\":\"42\"},{\"gallery_id\":\"4\"},{\"image\":\"2ab2a11cf1c786b3c379bdd6df4d36a90.jpeg\"},{\"created_at\":\"2021-03-16 21:32:52\"},{\"updated_at\":\"2021-03-16 21:32:52\"},{\"order\":\"8\"},{\"id\":\"41\"},{\"gallery_id\":\"4\"},{\"image\":\"1ab2a11cf1c786b3c379bdd6df4d36a90.jpeg\"},{\"created_at\":\"2021-03-16 21:32:52\"},{\"updated_at\":\"2021-03-16 21:32:52\"},{\"order\":\"7\"},{\"id\":\"40\"},{\"gallery_id\":\"4\"},{\"image\":\"0ab2a11cf1c786b3c379bdd6df4d36a90.jpeg\"},{\"created_at\":\"2021-03-16 21:32:52\"},{\"updated_at\":\"2021-03-16 21:32:52\"},{\"order\":\"6\"},{\"id\":\"39\"},{\"gallery_id\":\"4\"},{\"image\":\"41a7969e6e702c9bf1248216aabee4813.jpeg\"},{\"created_at\":\"2021-03-16 21:31:59\"},{\"updated_at\":\"2021-03-16 21:31:59\"},{\"order\":\"5\"},{\"id\":\"38\"},{\"gallery_id\":\"4\"},{\"image\":\"31a7969e6e702c9bf1248216aabee4813.jpeg\"},{\"created_at\":\"2021-03-16 21:31:59\"},{\"updated_at\":\"2021-03-16 21:31:59\"},{\"order\":\"4\"},{\"id\":\"37\"},{\"gallery_id\":\"4\"},{\"image\":\"21a7969e6e702c9bf1248216aabee4813.jpeg\"},{\"created_at\":\"2021-03-16 21:31:59\"},{\"updated_at\":\"2021-03-16 21:31:59\"},{\"order\":\"3\"},{\"id\":\"36\"},{\"gallery_id\":\"4\"},{\"image\":\"11a7969e6e702c9bf1248216aabee4813.jpeg\"},{\"created_at\":\"2021-03-16 21:31:59\"},{\"updated_at\":\"2021-03-16 21:31:59\"},{\"order\":\"2\"},{\"id\":\"35\"},{\"gallery_id\":\"4\"},{\"image\":\"01a7969e6e702c9bf1248216aabee4813.jpeg\"},{\"created_at\":\"2021-03-16 21:31:59\"},{\"updated_at\":\"2021-03-16 21:31:59\"},{\"order\":\"1\"}]', '2021-03-06 10:04:56', '2021-03-17 02:34:06'),
(116, 26, 46, 'broshura', 'file', '\"1161615030272.pdf\"', '2021-03-06 10:11:09', '2021-03-06 10:31:12'),
(117, 27, 47, 'title', 'text', '{\"al\":\"Nd\\u00ebrtim i ul\\u00ebt\",\"en\":\"Low Construction\"}', '2021-03-06 10:39:16', '2021-03-06 10:40:30'),
(118, 27, 48, 'background', 'file', '\"1181615933472.jpeg\"', '2021-03-06 10:39:16', '2021-03-17 03:24:32'),
(119, 28, 49, 'image', 'file', '\"1191615933516.jpeg\"', '2021-03-06 10:39:36', '2021-03-17 03:25:16'),
(120, 28, 50, 'description', 'editor', '{\"al\":\"<ul>\\n\\t<li>rrug&euml; (rrug&euml;, rrethrotullime, shtigje p&euml;r bi\\u04abikleta...)<\\/li>\\n\\t<li>sisteme t&euml; rrjeteve komunale (kanalizim, pajisje t&euml; pastrimit, uj&euml;sjell&euml;s, tubacione gazi, linjat e telekomunikacionit...)<\\/li>\\n\\t<li>&nbsp;objekte sportivo rekreative (parqe dhe fusha sporti, qendra rekreative...)<\\/li>\\n\\t<li>&nbsp;rregullime t&euml; ambientit t&euml; jasht&euml;m (parkingjeve, rregullimi i trojeve, ndri\\u04abimit publik, parqeve, shesheve...)<\\/li>\\n<\\/ul>\",\"en\":\"<ul>\\n\\t<li>roads (roads, roundabouts, bicycle paths ...)<\\/li>\\n\\t<li>utility network systems (sewerage, cleaning equipment, water supply, gas pipelines, telecommunication lines ...)<\\/li>\\n\\t<li>recreational sports facilities (parks and sports fields, recreation centers ...)<\\/li>\\n\\t<li>exterior arrangements (parking lots, landscaping, public lighting, parks, squares ...)<\\/li>\\n<\\/ul>\"}', '2021-03-06 10:39:36', '2021-03-17 02:17:21'),
(121, 28, 51, 'gallery', 'gallery', '[{\"id\":\"34\"},{\"gallery_id\":\"5\"},{\"image\":\"3c850e4da2f591343d273e99a8700d9af.jpeg\"},{\"created_at\":\"2021-03-16 21:13:38\"},{\"updated_at\":\"2021-03-16 21:13:38\"},{\"order\":\"4\"},{\"id\":\"33\"},{\"gallery_id\":\"5\"},{\"image\":\"2c850e4da2f591343d273e99a8700d9af.jpeg\"},{\"created_at\":\"2021-03-16 21:13:38\"},{\"updated_at\":\"2021-03-16 21:13:38\"},{\"order\":\"3\"},{\"id\":\"32\"},{\"gallery_id\":\"5\"},{\"image\":\"1c850e4da2f591343d273e99a8700d9af.jpeg\"},{\"created_at\":\"2021-03-16 21:13:38\"},{\"updated_at\":\"2021-03-16 21:13:38\"},{\"order\":\"2\"},{\"id\":\"31\"},{\"gallery_id\":\"5\"},{\"image\":\"0c850e4da2f591343d273e99a8700d9af.jpeg\"},{\"created_at\":\"2021-03-16 21:13:38\"},{\"updated_at\":\"2021-03-16 21:13:38\"},{\"order\":\"1\"}]', '2021-03-06 10:39:36', '2021-03-17 02:15:19'),
(122, 28, 52, 'broshura', 'file', '\"1221615030809.pdf\"', '2021-03-06 10:39:37', '2021-03-06 10:40:09'),
(123, 29, 53, 'title', 'text', '{\"al\":\"Lika Asfalt\",\"en\":\"Lika Asphalt\"}', '2021-03-06 10:43:59', '2021-03-06 10:52:03'),
(124, 30, 55, 'image', 'file', '\"1241615931524.jpg\"', '2021-03-06 10:45:21', '2021-03-17 02:52:04'),
(125, 30, 56, 'description', 'editor', '{\"al\":\"<p>B&euml;jm prodhimin e t&euml; gjitha lloj&euml;ve t&euml; asfalltit t&euml; gatsh&euml;m sipas standart&euml;ve<\\/p>\",\"en\":\"<p>We produce all types of asphalt ready according to standards<\\/p>\"}', '2021-03-06 10:45:21', '2021-03-17 03:27:22'),
(126, 30, 57, 'gallery', 'gallery', NULL, '2021-03-06 10:45:21', '2021-03-17 02:52:04'),
(127, 29, 59, 'background', 'file', '\"1271615931567.jpg\"', '2021-03-06 10:53:39', '2021-03-17 02:52:47'),
(128, 30, 60, 'broshura', 'file', '\"1281615031661.pdf\"', '2021-03-06 10:54:12', '2021-03-06 10:54:21'),
(129, 31, 61, 'title', 'text', '{\"al\":\"Lika Beton\",\"en\":\"Lika Concrete\"}', '2021-03-06 11:25:22', '2021-03-06 11:25:28'),
(130, 31, 62, 'background', 'file', '\"621615033522.jpg\"', '2021-03-06 11:25:22', '2021-03-06 11:25:22'),
(131, 32, 63, 'image', 'file', '\"1311615033591.jpg\"', '2021-03-06 11:25:48', '2021-03-06 11:26:31'),
(132, 32, 64, 'description', 'editor', '{\"al\":\"<p>Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget.. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget..<\\/p>\\n\\n<p>Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget.. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget.<\\/p>\\n\\n<p>Aenean suscipit eget mi act<\\/p>\\n\\n<p>Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget.. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget..<\\/p>\\n\\n<p>Reporting and Cost Control<\\/p>\\n\\n<p>Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget.. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget..<\\/p>\",\"en\":\"<p>Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget.. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget..<\\/p>\\n\\n<p>Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget.. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget.<\\/p>\\n\\n<p>Aenean suscipit eget mi act<\\/p>\\n\\n<p>Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget.. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget..<\\/p>\\n\\n<p>Reporting and Cost Control<\\/p>\\n\\n<p>Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget.. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget..<\\/p>\"}', '2021-03-06 11:25:48', '2021-03-06 11:25:48'),
(133, 32, 65, 'gallery', 'gallery', '[{\"id\":\"19\"},{\"gallery_id\":\"7\"},{\"image\":\"26108943e0e0dd4c92da79e32f84e4aa6.jpg\"},{\"created_at\":\"2021-03-06 12:26:17\"},{\"updated_at\":\"2021-03-06 12:26:17\"},{\"order\":\"3\"},{\"id\":\"18\"},{\"gallery_id\":\"7\"},{\"image\":\"16108943e0e0dd4c92da79e32f84e4aa6.jpg\"},{\"created_at\":\"2021-03-06 12:26:17\"},{\"updated_at\":\"2021-03-06 12:26:17\"},{\"order\":\"2\"},{\"id\":\"17\"},{\"gallery_id\":\"7\"},{\"image\":\"06108943e0e0dd4c92da79e32f84e4aa6.jpg\"},{\"created_at\":\"2021-03-06 12:26:17\"},{\"updated_at\":\"2021-03-06 12:26:17\"},{\"order\":\"1\"}]', '2021-03-06 11:25:48', '2021-03-06 11:26:31'),
(134, 32, 67, 'broshura', 'file', '\"1341615033725.pdf\"', '2021-03-06 11:28:31', '2021-03-06 11:28:45'),
(135, 33, 68, 'title', 'text', '{\"al\":\"Lika Gurthyes\",\"en\":\"Lika Stone\"}', '2021-03-06 11:32:20', '2021-03-06 11:35:19'),
(136, 33, 69, 'background', 'file', '\"691615033940.jpg\"', '2021-03-06 11:32:20', '2021-03-06 11:32:20'),
(137, 34, 70, 'image', 'file', '\"701615034019.jpg\"', '2021-03-06 11:33:39', '2021-03-06 11:33:39'),
(138, 34, 71, 'description', 'editor', '{\"al\":\"<p>Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget.. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget..<\\/p>\\n\\n<p>Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget.. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget.<\\/p>\\n\\n<p>Aenean suscipit eget mi act<\\/p>\\n\\n<p>Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget.. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget..<\\/p>\\n\\n<p>Reporting and Cost Control<\\/p>\\n\\n<p>Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget.. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget..<\\/p>\",\"en\":\"<p>Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget.. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget..<\\/p>\\n\\n<p>Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget.. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget.<\\/p>\\n\\n<p>Aenean suscipit eget mi act<\\/p>\\n\\n<p>Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget.. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget..<\\/p>\\n\\n<p>Reporting and Cost Control<\\/p>\\n\\n<p>Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget.. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget..<\\/p>\"}', '2021-03-06 11:33:39', '2021-03-06 11:34:09'),
(139, 34, 72, 'gallery', 'gallery', '[{\"id\":\"22\"},{\"gallery_id\":\"8\"},{\"image\":\"2c8dcc1279ab4d99d4b0f6fb20da76618.jpg\"},{\"created_at\":\"2021-03-06 12:34:12\"},{\"updated_at\":\"2021-03-06 12:34:12\"},{\"order\":\"3\"},{\"id\":\"21\"},{\"gallery_id\":\"8\"},{\"image\":\"1c8dcc1279ab4d99d4b0f6fb20da76618.jpg\"},{\"created_at\":\"2021-03-06 12:34:12\"},{\"updated_at\":\"2021-03-06 12:34:12\"},{\"order\":\"2\"},{\"id\":\"20\"},{\"gallery_id\":\"8\"},{\"image\":\"0c8dcc1279ab4d99d4b0f6fb20da76618.jpg\"},{\"created_at\":\"2021-03-06 12:34:12\"},{\"updated_at\":\"2021-03-06 12:34:12\"},{\"order\":\"1\"}]', '2021-03-06 11:33:39', '2021-03-06 11:34:19'),
(140, 34, 74, 'broshura', 'file', '\"1401615034160.pdf\"', '2021-03-06 11:35:44', '2021-03-06 11:36:00'),
(141, 35, 75, 'title', 'text', '{\"al\":\"Mir\\u00ebmbajtja rrugore n\\u00eb ver\\u00eb dhe dim\\u00ebr\",\"en\":\"Lika Street maintenance \\/ Summer and Winter\"}', '2021-03-06 11:44:50', '2021-03-06 11:45:20'),
(142, 35, 76, 'background', 'file', '\"761615034690.jpg\"', '2021-03-06 11:44:50', '2021-03-06 11:44:50'),
(143, 36, 77, 'image', 'file', '\"1431615034865.jpg\"', '2021-03-06 11:47:18', '2021-03-06 11:47:45'),
(144, 36, 78, 'description', 'editor', '{\"al\":\"<p>Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget.. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget..<\\/p>\\n\\n<p>Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget.. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget.<\\/p>\\n\\n<p>Aenean suscipit eget mi act<\\/p>\\n\\n<p>Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget.. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget..<\\/p>\\n\\n<p>Reporting and Cost Control<\\/p>\\n\\n<p>Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget.. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget..<\\/p>\",\"en\":\"<p>Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget.. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget..<\\/p>\\n\\n<p>Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget.. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget.<\\/p>\\n\\n<p>Aenean suscipit eget mi act<\\/p>\\n\\n<p>Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget.. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget..<\\/p>\\n\\n<p>Reporting and Cost Control<\\/p>\\n\\n<p>Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget.. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget..<\\/p>\"}', '2021-03-06 11:47:18', '2021-03-06 11:47:21'),
(145, 36, 79, 'gallery', 'gallery', '[{\"id\":\"25\"},{\"gallery_id\":\"9\"},{\"image\":\"2c4b49f2771c0eb0fc40a5f32ff1bbd7b.jpg\"},{\"created_at\":\"2021-03-06 12:47:45\"},{\"updated_at\":\"2021-03-06 12:47:45\"},{\"order\":\"3\"},{\"id\":\"24\"},{\"gallery_id\":\"9\"},{\"image\":\"1c4b49f2771c0eb0fc40a5f32ff1bbd7b.jpg\"},{\"created_at\":\"2021-03-06 12:47:45\"},{\"updated_at\":\"2021-03-06 12:47:45\"},{\"order\":\"2\"},{\"id\":\"23\"},{\"gallery_id\":\"9\"},{\"image\":\"0c4b49f2771c0eb0fc40a5f32ff1bbd7b.jpg\"},{\"created_at\":\"2021-03-06 12:47:45\"},{\"updated_at\":\"2021-03-06 12:47:45\"},{\"order\":\"1\"}]', '2021-03-06 11:47:18', '2021-03-06 11:50:01'),
(146, 36, 80, 'broshura', 'file', '\"1461615035001.pdf\"', '2021-03-06 11:49:52', '2021-03-06 11:50:01'),
(156, 1, 81, 'title4', 'text', '{\"al\":\"ND\\u00cbRTIM I LART\\u00cb\",\"en\":\"HIGH CONSTRUCTION\"}', '2021-03-15 11:21:04', '2021-03-17 00:56:56'),
(157, 1, 82, 'description4', 'editor', '{\"al\":\"<p>Me plot&euml;simin&nbsp;e&nbsp;standardeve nd&euml;rtimore me&nbsp;norma&nbsp;t&euml; nd&euml;rtimit modern dhe modeleve nd&euml;rkomb&euml;tare.<\\/p>\",\"en\":\"<p>Meeting construction standards with modern construction norms and international models.<\\/p>\"}', '2021-03-15 11:21:13', '2021-03-17 00:56:56'),
(158, 1, 83, 'title5', 'text', '{\"al\":\"ND\\u00cbRTIM I UL\\u00cbT\",\"en\":\"LOW CONSTRUCTION\"}', '2021-03-15 11:21:29', '2021-03-17 01:05:35'),
(159, 1, 84, 'description5', 'editor', '{\"al\":\"<p>B&euml;jm&euml; nd&euml;rtimin e t&euml; gjitha kategorive t&euml; rrug&euml;ve me infrastruktur&euml; p&euml;rcjell&euml;se<\\/p>\",\"en\":\"<p>We build all categories of roads with accompanying infrastructure<\\/p>\"}', '2021-03-15 11:21:37', '2021-03-17 01:07:38'),
(160, 1, 85, 'title6', 'text', '{\"al\":\"MIR\\u00cbMBAJTJA E RRUG\\u00cbVE VERORE DHE DIM\\u00cbRORE.\",\"en\":\"SUMMER AND WINTER ROAD MAINTENANCE\"}', '2021-03-15 11:21:46', '2021-03-17 01:12:10'),
(161, 1, 86, 'description6', 'editor', '{\"al\":\"<p>Mir&euml;mbajtja e rrug&euml;ve publike.<\\/p>\",\"en\":\"<p>Maintenance of public roads.<\\/p>\"}', '2021-03-15 11:21:58', '2021-03-17 01:12:10');

-- --------------------------------------------------------

--
-- Table structure for table `cms_pages_entities`
--

CREATE TABLE `cms_pages_entities` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slugable` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cms_pages_entities`
--

INSERT INTO `cms_pages_entities` (`id`, `slug`, `slugable`, `type_id`, `created_at`, `updated_at`, `order`) VALUES
(1, 'cover-1612190155', NULL, 1, '2021-02-01 13:35:55', '2021-02-01 13:35:55', NULL),
(3, 'projects-1613842637', '{\"al\":\"zgjerimi-i-rruges-nacionale-n-91-segmenti-dollc-gjakove-l3100\",\"en\":\"zgjerimi-i-rruges-nacionale-n-91-segmenti-dollc-gjakove-l3100\"}', 4, '2021-02-20 16:37:17', '2021-03-17 03:47:55', 2),
(6, 'projects-1614001177', '{\"al\":\"ndertimi-zgjerimi-i-rruges-r-107-qendrore-ne-decan\",\"en\":\"ndertimi-zgjerimi-i-rruges-r-107-qendrore-ne-decan\"}', 4, '2021-02-22 12:39:37', '2021-03-17 03:49:36', 1),
(7, 'we_provide-1614005710', NULL, 5, '2021-02-22 13:55:10', '2021-02-22 13:55:10', NULL),
(8, 'testimonial-1614005970', '{\"al\":\"joe-doe-2\",\"en\":\"joe-doe\"}', 6, '2021-02-22 13:59:30', '2021-02-23 10:26:15', 1),
(9, 'facebook_posts-1614079830', '{\"al\":\"cientific-results-conference-eduma-summer-2017\",\"en\":\"cientific-results-conference-eduma-summer-2017\"}', 7, '2021-02-23 10:30:30', '2021-02-23 10:30:30', 1),
(10, 'mission_vision-1614087709', NULL, 8, '2021-02-23 12:41:49', '2021-02-23 12:41:49', NULL),
(11, 'ceo_letter-1614088256', NULL, 9, '2021-02-23 12:50:56', '2021-02-23 12:50:56', NULL),
(12, 'partners-1614088632', '{\"al\":\"\",\"en\":\"1\"}', 10, '2021-02-23 12:57:12', '2021-02-23 12:57:12', 1),
(13, 'partners-1614088641', '{\"al\":\"\",\"en\":\"2\"}', 10, '2021-02-23 12:57:21', '2021-02-23 12:57:21', 2),
(14, 'partners-1614088651', '{\"al\":\"\",\"en\":\"3\"}', 10, '2021-02-23 12:57:31', '2021-02-23 12:57:31', 3),
(15, 'partners-1614088659', '{\"al\":\"\",\"en\":\"4\"}', 10, '2021-02-23 12:57:39', '2021-02-23 12:57:39', 4),
(16, 'partners-1614088668', '{\"al\":\"\",\"en\":\"5\"}', 10, '2021-02-23 12:57:48', '2021-02-23 12:57:48', 5),
(17, 'partners-1614088676', '{\"al\":\"\",\"en\":\"6\"}', 10, '2021-02-23 12:57:56', '2021-02-23 12:57:56', 6),
(18, 'partners-1614088685', '{\"al\":\"\",\"en\":\"7\"}', 10, '2021-02-23 12:58:05', '2021-02-23 12:58:05', 7),
(19, 'partners-1614088894', '{\"al\":\"\",\"en\":\"8\"}', 10, '2021-02-23 13:01:34', '2021-02-23 13:01:34', 8),
(20, 'partners-1614088902', '{\"al\":\"\",\"en\":\"9\"}', 10, '2021-02-23 13:01:42', '2021-02-23 13:01:42', 9),
(21, 'partners-1614088920', '{\"al\":\"\",\"en\":\"10\"}', 10, '2021-02-23 13:02:00', '2021-02-23 13:02:00', 10),
(22, 'career_m-1614089890', '{\"al\":\"kontabilist\",\"en\":\"accountant\"}', 11, '2021-02-23 13:18:10', '2021-02-23 13:18:59', 1),
(23, 'career_m-1614089905', '{\"al\":\"arkitekt\",\"en\":\"architect\"}', 11, '2021-02-23 13:18:25', '2021-02-23 13:18:38', 2),
(24, 'career_m-1614089953', '{\"al\":\"inxhinier\",\"en\":\"engineer\"}', 11, '2021-02-23 13:19:13', '2021-02-23 13:19:13', 3),
(25, 'cover_construction-1615028233', NULL, 12, '2021-03-06 09:57:13', '2021-03-06 09:57:13', NULL),
(26, 'const_body-1615028696', NULL, 13, '2021-03-06 10:04:56', '2021-03-06 10:04:56', NULL),
(27, 'cover_low-1615030756', NULL, 14, '2021-03-06 10:39:16', '2021-03-06 10:39:16', NULL),
(28, 'body_low-1615030776', NULL, 15, '2021-03-06 10:39:36', '2021-03-06 10:39:36', NULL),
(29, 'cover_asph-1615031039', NULL, 16, '2021-03-06 10:43:59', '2021-03-06 10:43:59', NULL),
(30, 'body_asph-1615031121', NULL, 17, '2021-03-06 10:45:21', '2021-03-06 10:45:21', NULL),
(31, 'cover_beton-1615033522', NULL, 18, '2021-03-06 11:25:22', '2021-03-06 11:25:22', NULL),
(32, 'body_bet-1615033548', NULL, 19, '2021-03-06 11:25:48', '2021-03-06 11:25:48', NULL),
(33, 'cover_stone-1615033940', NULL, 20, '2021-03-06 11:32:20', '2021-03-06 11:32:20', NULL),
(34, 'body_stone-1615034019', NULL, 21, '2021-03-06 11:33:39', '2021-03-06 11:33:39', NULL),
(35, 'cover_maint-1615034690', NULL, 22, '2021-03-06 11:44:50', '2021-03-06 11:44:50', NULL),
(36, 'body_maint-1615034838', NULL, 24, '2021-03-06 11:47:18', '2021-03-06 11:47:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_pages_fields`
--

CREATE TABLE `cms_pages_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slugable` tinyint(1) NOT NULL DEFAULT '0',
  `type` enum('text','editor','numeric','date','email','file','gallery') COLLATE utf8_unicode_ci NOT NULL,
  `rule` enum('required') COLLATE utf8_unicode_ci NOT NULL,
  `page_type_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cms_pages_fields`
--

INSERT INTO `cms_pages_fields` (`id`, `label`, `name`, `model`, `slugable`, `type`, `rule`, `page_type_id`, `created_at`, `updated_at`) VALUES
(1, 'Titulli', 'title', '{\"al\":\"\",\"en\":\"\"}', 1, 'text', 'required', 1, '2021-02-01 13:35:23', '2021-02-01 13:35:23'),
(2, 'Pershkrimi', 'description', '{\"al\":\"\",\"en\":\"\"}', 0, 'editor', 'required', 1, '2021-02-01 13:35:31', '2021-02-01 13:35:31'),
(4, 'Title', 'title', '{\"al\":\"\",\"en\":\"\"}', 1, 'text', 'required', 3, '2021-02-20 16:15:02', '2021-02-20 16:15:02'),
(5, 'Project Name', 'name', '{\"al\":\"\",\"en\":\"\"}', 1, 'text', 'required', 4, '2021-02-20 16:31:01', '2021-02-20 16:31:01'),
(6, 'Client', 'client', '{\"al\":\"\",\"en\":\"\"}', 0, 'text', 'required', 4, '2021-02-20 16:31:13', '2021-02-20 16:31:13'),
(7, 'Location', 'location', '{\"al\":\"\",\"en\":\"\"}', 0, 'text', 'required', 4, '2021-02-20 16:31:25', '2021-02-20 16:31:25'),
(8, 'Year', 'year', '\"year\"', 0, 'date', 'required', 4, '2021-02-20 16:31:48', '2021-02-20 16:31:48'),
(9, 'Value', 'value', '{\"al\":\"\",\"en\":\"\"}', 0, 'text', 'required', 4, '2021-02-20 16:32:04', '2021-02-20 16:32:04'),
(10, 'Description', 'description', '{\"al\":\"\",\"en\":\"\"}', 0, 'editor', 'required', 4, '2021-02-20 16:32:20', '2021-02-20 16:32:20'),
(11, 'Gallery', 'gallery', '\"gallery\"', 0, 'gallery', 'required', 4, '2021-02-20 16:32:36', '2021-02-20 16:32:36'),
(12, 'Image Main', 'main_image', '\"main_image\"', 0, 'file', 'required', 4, '2021-02-20 16:33:48', '2021-02-20 16:33:48'),
(13, 'Title 2', 'title2', '{\"al\":\"\",\"en\":\"\"}', 0, 'text', 'required', 1, '2021-02-22 13:37:00', '2021-02-22 13:37:00'),
(14, 'Pershkrimi 2', 'description2', '{\"al\":\"\",\"en\":\"\"}', 0, 'editor', 'required', 1, '2021-02-22 13:37:13', '2021-02-22 13:37:13'),
(15, 'Titulli 3', 'title3', '{\"al\":\"\",\"en\":\"\"}', 0, 'text', 'required', 1, '2021-02-22 13:37:24', '2021-02-22 13:37:24'),
(16, 'Pershkrimi 3', 'description3', '{\"al\":\"\",\"en\":\"\"}', 0, 'editor', 'required', 1, '2021-02-22 13:37:45', '2021-02-22 13:37:45'),
(17, 'Title', 'title', '{\"al\":\"\",\"en\":\"\"}', 1, 'text', 'required', 5, '2021-02-22 13:54:49', '2021-02-22 13:54:49'),
(18, 'Description', 'description', '{\"al\":\"\",\"en\":\"\"}', 0, 'editor', 'required', 5, '2021-02-22 13:54:56', '2021-02-22 13:54:56'),
(19, 'Name', 'name', '{\"al\":\"\",\"en\":\"\"}', 1, 'text', 'required', 6, '2021-02-22 13:57:42', '2021-02-22 13:57:42'),
(20, 'Job Position', 'job_position', '{\"al\":\"\",\"en\":\"\"}', 0, 'text', 'required', 6, '2021-02-22 13:57:57', '2021-02-22 13:57:57'),
(21, 'Description', 'description', '{\"al\":\"\",\"en\":\"\"}', 0, 'editor', 'required', 6, '2021-02-22 13:58:08', '2021-02-22 13:58:08'),
(22, 'Image', 'image', '\"image\"', 0, 'file', 'required', 6, '2021-02-22 13:58:13', '2021-02-22 13:58:13'),
(23, 'Title', 'title', '{\"al\":\"\",\"en\":\"\"}', 1, 'text', 'required', 7, '2021-02-23 10:28:51', '2021-02-23 10:28:51'),
(24, 'Date', 'date', '{\"al\":\"\",\"en\":\"\"}', 0, 'text', 'required', 7, '2021-02-23 10:28:57', '2021-02-23 10:28:57'),
(25, 'Author', 'author', '{\"al\":\"\",\"en\":\"\"}', 0, 'text', 'required', 7, '2021-02-23 10:29:04', '2021-02-23 10:29:04'),
(26, 'Image', 'image', '\"image\"', 0, 'file', 'required', 7, '2021-02-23 10:29:12', '2021-02-23 10:29:12'),
(27, 'Facebook Link', 'link', '{\"al\":\"\",\"en\":\"\"}', 0, 'text', 'required', 7, '2021-02-23 10:29:22', '2021-02-23 10:29:22'),
(28, 'Title', 'title', '{\"al\":\"\",\"en\":\"\"}', 1, 'text', 'required', 8, '2021-02-23 12:24:27', '2021-02-23 12:24:27'),
(29, 'Description', 'description', '{\"al\":\"\",\"en\":\"\"}', 0, 'editor', 'required', 8, '2021-02-23 12:24:35', '2021-02-23 12:24:35'),
(30, 'Image', 'image', '\"image\"', 0, 'file', 'required', 8, '2021-02-23 12:24:40', '2021-02-23 12:24:40'),
(31, 'Title 2', 'title2', '{\"al\":\"\",\"en\":\"\"}', 0, 'text', 'required', 8, '2021-02-23 12:24:51', '2021-02-23 12:24:51'),
(32, 'Description 2', 'description2', '{\"al\":\"\",\"en\":\"\"}', 0, 'editor', 'required', 8, '2021-02-23 12:25:05', '2021-02-23 12:25:05'),
(33, 'Image 2', 'image2', '\"image2\"', 0, 'file', 'required', 8, '2021-02-23 12:26:05', '2021-02-23 12:26:05'),
(34, 'Name', 'name', '{\"al\":\"\",\"en\":\"\"}', 1, 'text', 'required', 9, '2021-02-23 12:49:49', '2021-02-23 12:49:49'),
(35, 'job Position', 'job_position', '{\"al\":\"\",\"en\":\"\"}', 0, 'text', 'required', 9, '2021-02-23 12:50:01', '2021-02-23 12:50:01'),
(36, 'Paragraph', 'paragraph', '{\"al\":\"\",\"en\":\"\"}', 0, 'editor', 'required', 9, '2021-02-23 12:50:10', '2021-02-23 12:50:10'),
(37, 'Ceo Image', 'image', '\"image\"', 0, 'file', 'required', 9, '2021-02-23 12:50:17', '2021-02-23 12:50:17'),
(38, 'Name', 'name', '{\"al\":\"\",\"en\":\"\"}', 1, 'text', 'required', 10, '2021-02-23 12:56:51', '2021-02-23 12:56:51'),
(39, 'Logo', 'logo', '\"logo\"', 0, 'file', 'required', 10, '2021-02-23 12:56:58', '2021-02-23 12:56:58'),
(40, 'Title', 'title', '{\"al\":\"\",\"en\":\"\"}', 1, 'text', 'required', 11, '2021-02-23 13:16:44', '2021-02-23 13:16:44'),
(41, 'Title', 'title', '{\"al\":\"\",\"en\":\"\"}', 1, 'text', 'required', 12, '2021-03-06 09:55:23', '2021-03-06 09:55:23'),
(42, 'Background', 'background', '\"background\"', 0, 'file', 'required', 12, '2021-03-06 09:55:47', '2021-03-06 09:55:47'),
(43, 'Image', 'image', '\"image\"', 0, 'file', 'required', 13, '2021-03-06 10:01:28', '2021-03-06 10:01:28'),
(44, 'Description', 'description', '{\"al\":\"\",\"en\":\"\"}', 0, 'editor', 'required', 13, '2021-03-06 10:01:38', '2021-03-06 10:01:38'),
(45, 'Gallery', 'gallery', '\"gallery\"', 0, 'gallery', 'required', 13, '2021-03-06 10:04:06', '2021-03-06 10:04:06'),
(46, 'Brochure', 'broshura', '\"broshura\"', 0, 'file', 'required', 13, '2021-03-06 10:11:09', '2021-03-06 10:11:09'),
(47, 'Title', 'title', '{\"al\":\"\",\"en\":\"\"}', 1, 'text', 'required', 14, '2021-03-06 10:35:28', '2021-03-06 10:35:28'),
(48, 'Background', 'background', '\"background\"', 0, 'file', 'required', 14, '2021-03-06 10:35:36', '2021-03-06 10:35:36'),
(49, 'Image', 'image', '\"image\"', 1, 'file', 'required', 15, '2021-03-06 10:36:24', '2021-03-06 10:36:24'),
(50, 'Description', 'description', '{\"al\":\"\",\"en\":\"\"}', 0, 'editor', 'required', 15, '2021-03-06 10:36:36', '2021-03-06 10:36:36'),
(51, 'Gallery', 'gallery', '\"gallery\"', 0, 'gallery', 'required', 15, '2021-03-06 10:36:45', '2021-03-06 10:36:45'),
(52, 'Brochure', 'broshura', '\"broshura\"', 0, 'file', 'required', 15, '2021-03-06 10:37:01', '2021-03-06 10:37:01'),
(53, 'Title', 'title', '{\"al\":\"\",\"en\":\"\"}', 1, 'text', 'required', 16, '2021-03-06 10:42:15', '2021-03-06 10:42:15'),
(55, 'Image', 'image', '\"image\"', 1, 'file', 'required', 17, '2021-03-06 10:43:00', '2021-03-06 10:43:00'),
(56, 'Description', 'description', '{\"al\":\"\",\"en\":\"\"}', 0, 'editor', 'required', 17, '2021-03-06 10:43:10', '2021-03-06 10:43:10'),
(57, 'Gallery', 'gallery', '\"gallery\"', 0, 'gallery', 'required', 17, '2021-03-06 10:43:23', '2021-03-06 10:43:23'),
(59, 'Background', 'background', '\"background\"', 0, 'file', 'required', 16, '2021-03-06 10:53:39', '2021-03-06 10:53:39'),
(60, 'Brochure', 'broshura', '\"broshura\"', 0, 'file', 'required', 17, '2021-03-06 10:54:12', '2021-03-06 10:54:12'),
(61, 'Title', 'title', '{\"al\":\"\",\"en\":\"\"}', 1, 'text', 'required', 18, '2021-03-06 11:23:02', '2021-03-06 11:23:02'),
(62, 'Background', 'background', '\"background\"', 0, 'file', 'required', 18, '2021-03-06 11:23:10', '2021-03-06 11:23:10'),
(63, 'Image', 'image', '\"image\"', 1, 'file', 'required', 19, '2021-03-06 11:23:39', '2021-03-06 11:23:39'),
(64, 'Description', 'description', '{\"al\":\"\",\"en\":\"\"}', 0, 'editor', 'required', 19, '2021-03-06 11:23:49', '2021-03-06 11:23:49'),
(65, 'Gallery', 'gallery', '\"gallery\"', 0, 'gallery', 'required', 19, '2021-03-06 11:23:58', '2021-03-06 11:23:58'),
(67, 'Brochure', 'broshura', '\"broshura\"', 0, 'file', 'required', 19, '2021-03-06 11:28:31', '2021-03-06 11:28:31'),
(68, 'Title', 'title', '{\"al\":\"\",\"en\":\"\"}', 1, 'text', 'required', 20, '2021-03-06 11:30:14', '2021-03-06 11:30:14'),
(69, 'Background', 'background', '\"background\"', 0, 'file', 'required', 20, '2021-03-06 11:30:22', '2021-03-06 11:30:22'),
(70, 'Image', 'image', '\"image\"', 1, 'file', 'required', 21, '2021-03-06 11:32:53', '2021-03-06 11:32:53'),
(71, 'Description', 'description', '{\"al\":\"\",\"en\":\"\"}', 0, 'editor', 'required', 21, '2021-03-06 11:33:04', '2021-03-06 11:33:04'),
(72, 'Gallery', 'gallery', '\"gallery\"', 0, 'gallery', 'required', 21, '2021-03-06 11:33:14', '2021-03-06 11:33:14'),
(74, 'Brochure', 'broshura', '\"broshura\"', 0, 'file', 'required', 21, '2021-03-06 11:35:44', '2021-03-06 11:35:44'),
(75, 'Title', 'title', '{\"al\":\"\",\"en\":\"\"}', 1, 'text', 'required', 22, '2021-03-06 11:38:52', '2021-03-06 11:38:52'),
(76, 'Background', 'background', '\"background\"', 0, 'file', 'required', 22, '2021-03-06 11:39:00', '2021-03-06 11:39:00'),
(77, 'Image', 'image', '\"image\"', 1, 'file', 'required', 24, '2021-03-06 11:46:38', '2021-03-06 11:46:38'),
(78, 'Description', 'description', '{\"al\":\"\",\"en\":\"\"}', 0, 'editor', 'required', 24, '2021-03-06 11:46:54', '2021-03-06 11:46:54'),
(79, 'Gallery', 'gallery', '\"gallery\"', 0, 'gallery', 'required', 24, '2021-03-06 11:47:04', '2021-03-06 11:47:04'),
(80, 'Brochure', 'broshura', '\"broshura\"', 0, 'file', 'required', 24, '2021-03-06 11:49:52', '2021-03-06 11:49:52'),
(81, 'Titulli 4', 'title4', '{\"al\":\"\",\"en\":\"\"}', 0, 'text', 'required', 1, '2021-03-15 11:21:04', '2021-03-15 11:21:04'),
(82, 'Pershkrimi 4', 'description4', '{\"al\":\"\",\"en\":\"\"}', 0, 'editor', 'required', 1, '2021-03-15 11:21:13', '2021-03-15 11:21:13'),
(83, 'Titulli 5', 'title5', '{\"al\":\"\",\"en\":\"\"}', 0, 'text', 'required', 1, '2021-03-15 11:21:29', '2021-03-15 11:21:29'),
(84, 'Pershkrimi 5', 'description5', '{\"al\":\"\",\"en\":\"\"}', 0, 'editor', 'required', 1, '2021-03-15 11:21:37', '2021-03-15 11:21:37'),
(85, 'Titulli 6', 'title6', '{\"al\":\"\",\"en\":\"\"}', 0, 'text', 'required', 1, '2021-03-15 11:21:46', '2021-03-15 11:21:46'),
(86, 'Pershkrimi 6', 'description6', '{\"al\":\"\",\"en\":\"\"}', 0, 'editor', 'required', 1, '2021-03-15 11:21:58', '2021-03-15 11:21:58');

-- --------------------------------------------------------

--
-- Table structure for table `cms_pages_types`
--

CREATE TABLE `cms_pages_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('one','many') COLLATE utf8_unicode_ci NOT NULL,
  `edit` tinyint(1) NOT NULL DEFAULT '0',
  `page_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cms_pages_types`
--

INSERT INTO `cms_pages_types` (`id`, `name`, `slug`, `type`, `edit`, `page_id`, `created_at`, `updated_at`) VALUES
(1, 'Cover', 'cover', 'one', 1, 1, '2021-02-01 13:35:09', '2021-02-01 13:35:55'),
(3, 'Cover', 'cover_proje', 'one', 0, 4, '2021-02-20 16:14:53', '2021-02-20 16:14:53'),
(4, 'Projects', 'projects', 'many', 0, 4, '2021-02-20 16:15:26', '2021-02-20 16:15:26'),
(5, 'We Provide', 'we_provide', 'one', 1, 1, '2021-02-22 13:54:39', '2021-02-22 13:55:10'),
(6, 'Testimonial', 'testimonial', 'many', 0, 1, '2021-02-22 13:57:27', '2021-02-22 13:57:27'),
(7, 'Facebook Posts', 'facebook_posts', 'many', 0, 1, '2021-02-23 10:27:57', '2021-02-23 10:27:57'),
(8, 'Mission & Vision', 'mission_vision', 'one', 1, 2, '2021-02-23 12:24:19', '2021-02-23 12:41:49'),
(9, 'CEO Letter', 'ceo_letter', 'one', 1, 2, '2021-02-23 12:49:38', '2021-02-23 12:50:56'),
(10, 'Partners', 'partners', 'many', 0, 2, '2021-02-23 12:56:44', '2021-02-23 12:56:44'),
(11, 'Career', 'career_m', 'many', 0, 5, '2021-02-23 13:16:34', '2021-02-23 13:16:34'),
(12, 'Cover', 'cover_construction', 'one', 1, 7, '2021-03-06 09:55:10', '2021-03-06 09:57:13'),
(13, 'Body', 'const_body', 'one', 1, 7, '2021-03-06 10:01:06', '2021-03-06 10:04:56'),
(14, 'Cover', 'cover_low', 'one', 1, 8, '2021-03-06 10:35:16', '2021-03-06 10:39:16'),
(15, 'Body', 'body_low', 'one', 1, 8, '2021-03-06 10:36:06', '2021-03-06 10:39:36'),
(16, 'Cover', 'cover_asph', 'one', 1, 9, '2021-03-06 10:42:07', '2021-03-06 10:43:59'),
(17, 'Body', 'body_asph', 'one', 1, 9, '2021-03-06 10:42:47', '2021-03-06 10:45:20'),
(18, 'Cover', 'cover_beton', 'one', 1, 10, '2021-03-06 11:22:47', '2021-03-06 11:25:22'),
(19, 'Body', 'body_bet', 'one', 1, 10, '2021-03-06 11:23:26', '2021-03-06 11:25:48'),
(20, 'Cover', 'cover_stone', 'one', 1, 11, '2021-03-06 11:30:00', '2021-03-06 11:32:20'),
(21, 'Body', 'body_stone', 'one', 1, 11, '2021-03-06 11:32:37', '2021-03-06 11:33:39'),
(22, 'Cover', 'cover_maint', 'one', 1, 12, '2021-03-06 11:38:44', '2021-03-06 11:44:50'),
(24, 'Body', 'body_maint', 'one', 1, 12, '2021-03-06 11:46:11', '2021-03-06 11:47:18');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_11_11_154823_eg_languages', 1),
(4, '2017_11_12_113731_cms_pages', 1),
(5, '2017_11_12_213001_cms_page_type', 1),
(6, '2017_11_12_214115_cms_pages_fields', 1),
(7, '2017_11_19_142952_cms_page_entities', 1),
(8, '2017_11_19_143029_cms_page_collections', 1),
(9, '2017_12_06_180521_cms_gallery_entity', 1),
(10, '2017_12_06_180534_cms_gallery_images', 1),
(11, '2018_01_09_124118_alter_order_column', 1),
(12, '2018_02_05_165314_create_tbl_rel_type', 1),
(13, '2018_02_05_165323_create_tbl_rel_entry', 1),
(14, '2018_02_06_164146_create_building_table', 1),
(15, '2018_02_06_165641_create_building_relations', 1),
(16, '2018_02_07_182214_bld_gallery', 1),
(17, '2018_02_14_182710_seed_building', 1),
(18, '2018_02_15_172511_order_bld_building', 1),
(19, '2018_02_18_224258_order_price_building', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rel_entry`
--

CREATE TABLE `rel_entry` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rel_type`
--

CREATE TABLE `rel_type` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `field_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('multiple','one') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `rel_type`
--

INSERT INTO `rel_type` (`id`, `name`, `field_name`, `type`, `created_at`, `updated_at`) VALUES
(1, 'Location', 'location', 'one', '2021-01-19 20:59:16', '2021-01-19 20:59:16'),
(2, 'Type Of Project', 'type-of-project', 'one', '2021-01-19 20:59:16', '2021-01-19 20:59:16'),
(3, 'Status of Project', 'status-of-project', 'one', '2021-01-19 20:59:16', '2021-01-19 20:59:16'),
(4, 'Fitted', 'fitted', 'one', '2021-01-19 20:59:16', '2021-01-19 20:59:16'),
(5, 'Tags', 'tags', 'multiple', '2021-01-19 20:59:16', '2021-01-19 20:59:16');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin@admin.com', '$2y$10$w4Bt.EOH.tQPrb9dR4797.3fNsOtYta4K4f9ahxbwF3YhdhIxMn6e', 'Yuu2GvY4TjH3kJwNAuMjtwvtYQyUtpRaivEPLY8uX8DAJgFmp0CTOx644qh8', '2021-01-19 20:59:15', '2021-01-19 20:59:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bld_building`
--
ALTER TABLE `bld_building`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bld_gallery`
--
ALTER TABLE `bld_gallery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bld_gallery_building_id_foreign` (`building_id`);

--
-- Indexes for table `bld_relation`
--
ALTER TABLE `bld_relation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bld_relation_building_id_foreign` (`building_id`),
  ADD KEY `bld_relation_relation_id_foreign` (`relation_id`);

--
-- Indexes for table `cms_gallery_entity`
--
ALTER TABLE `cms_gallery_entity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_gallery_entity_field_id_foreign` (`field_id`),
  ADD KEY `cms_gallery_entity_entity_id_foreign` (`entity_id`);

--
-- Indexes for table `cms_gallery_images`
--
ALTER TABLE `cms_gallery_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_gallery_images_gallery_id_foreign` (`gallery_id`);

--
-- Indexes for table `cms_languages`
--
ALTER TABLE `cms_languages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cms_languages_key_unique` (`key`);

--
-- Indexes for table `cms_pages`
--
ALTER TABLE `cms_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_pages_collections`
--
ALTER TABLE `cms_pages_collections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_pages_collections_entity_id_foreign` (`entity_id`),
  ADD KEY `cms_pages_collections_field_id_foreign` (`field_id`);

--
-- Indexes for table `cms_pages_entities`
--
ALTER TABLE `cms_pages_entities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_pages_entities_type_id_foreign` (`type_id`);

--
-- Indexes for table `cms_pages_fields`
--
ALTER TABLE `cms_pages_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_pages_fields_page_type_id_foreign` (`page_type_id`);

--
-- Indexes for table `cms_pages_types`
--
ALTER TABLE `cms_pages_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_pages_types_page_id_foreign` (`page_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `rel_entry`
--
ALTER TABLE `rel_entry`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rel_entry_type_id_foreign` (`type_id`);

--
-- Indexes for table `rel_type`
--
ALTER TABLE `rel_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bld_building`
--
ALTER TABLE `bld_building`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bld_gallery`
--
ALTER TABLE `bld_gallery`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bld_relation`
--
ALTER TABLE `bld_relation`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_gallery_entity`
--
ALTER TABLE `cms_gallery_entity`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `cms_gallery_images`
--
ALTER TABLE `cms_gallery_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `cms_languages`
--
ALTER TABLE `cms_languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cms_pages`
--
ALTER TABLE `cms_pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `cms_pages_collections`
--
ALTER TABLE `cms_pages_collections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=162;

--
-- AUTO_INCREMENT for table `cms_pages_entities`
--
ALTER TABLE `cms_pages_entities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `cms_pages_fields`
--
ALTER TABLE `cms_pages_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT for table `cms_pages_types`
--
ALTER TABLE `cms_pages_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `rel_entry`
--
ALTER TABLE `rel_entry`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rel_type`
--
ALTER TABLE `rel_type`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bld_gallery`
--
ALTER TABLE `bld_gallery`
  ADD CONSTRAINT `bld_gallery_building_id_foreign` FOREIGN KEY (`building_id`) REFERENCES `bld_building` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `bld_relation`
--
ALTER TABLE `bld_relation`
  ADD CONSTRAINT `bld_relation_building_id_foreign` FOREIGN KEY (`building_id`) REFERENCES `bld_building` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `bld_relation_relation_id_foreign` FOREIGN KEY (`relation_id`) REFERENCES `rel_entry` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cms_gallery_entity`
--
ALTER TABLE `cms_gallery_entity`
  ADD CONSTRAINT `cms_gallery_entity_entity_id_foreign` FOREIGN KEY (`entity_id`) REFERENCES `cms_pages_entities` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cms_gallery_entity_field_id_foreign` FOREIGN KEY (`field_id`) REFERENCES `cms_pages_fields` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cms_gallery_images`
--
ALTER TABLE `cms_gallery_images`
  ADD CONSTRAINT `cms_gallery_images_gallery_id_foreign` FOREIGN KEY (`gallery_id`) REFERENCES `cms_gallery_entity` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cms_pages_collections`
--
ALTER TABLE `cms_pages_collections`
  ADD CONSTRAINT `cms_pages_collections_entity_id_foreign` FOREIGN KEY (`entity_id`) REFERENCES `cms_pages_entities` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cms_pages_collections_field_id_foreign` FOREIGN KEY (`field_id`) REFERENCES `cms_pages_fields` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cms_pages_entities`
--
ALTER TABLE `cms_pages_entities`
  ADD CONSTRAINT `cms_pages_entities_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `cms_pages_types` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cms_pages_fields`
--
ALTER TABLE `cms_pages_fields`
  ADD CONSTRAINT `cms_pages_fields_page_type_id_foreign` FOREIGN KEY (`page_type_id`) REFERENCES `cms_pages_types` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cms_pages_types`
--
ALTER TABLE `cms_pages_types`
  ADD CONSTRAINT `cms_pages_types_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `cms_pages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `rel_entry`
--
ALTER TABLE `rel_entry`
  ADD CONSTRAINT `rel_entry_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `rel_type` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
